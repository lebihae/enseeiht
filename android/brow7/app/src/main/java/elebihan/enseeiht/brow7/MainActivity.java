package elebihan.enseeiht.brow7;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    private static final String TAG = "brows7";
    private static final int SELECT_VIDEO = 100;

    public Uri pickedVideo;

    public MediaPlayer player;

    public int currentVideoPosition = 0;

    class BarUpdaterTask extends AsyncTask<Integer, Integer, Long> {
        @Override
        protected Long doInBackground(Integer... params) {
            while (!isCancelled()) {
                publishProgress();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return 0L;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SELECT_VIDEO && resultCode != 0) {
            TextView pickedUriText = findViewById(R.id.picked_video_uri);
            assert data != null;
            this.pickedVideo = Objects.requireNonNull(data.getData());
            pickedUriText.setText(this.pickedVideo.toString());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "feur");
        player = new MediaPlayer();
        setContentView(R.layout.activity_main);
        EditText urlField = findViewById(R.id.url);

        Button goButton = findViewById(R.id.go);
        goButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(urlField.getText().toString()));
            startActivity(intent);
        });

        Button selectMedia = findViewById(R.id.select_video);
        selectMedia.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setDataAndType(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "video/*");
            startActivityForResult(intent, SELECT_VIDEO);
        });

        Button playMedia = findViewById(R.id.play);
        playMedia.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Log.d(TAG, "Playing video " + MainActivity.this.pickedVideo);
            intent.setData(MainActivity.this.pickedVideo);
            startActivity(intent);
        });

        ImageButton playPause = findViewById(R.id.playpause);
        ImageButton playFromStart = findViewById(R.id.playfromstart);
        SurfaceView view = findViewById(R.id.videoSurface);
        view.getHolder().addCallback(this);

        playFromStart.setOnClickListener(v -> {
            try {
                if (player == null) return;
                player.reset();
                player.setDataSource(MainActivity.this.pickedVideo.getPath());
                player.prepare();
                player.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        playPause.setOnClickListener(v -> {
            if (player == null) return;
            if (player.isPlaying()) {
                player.pause();
            } else {
                player.start();
            }
        });

        SeekBar seekbar = findViewById(R.id.seekbar);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (player == null) return;
                player.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        currentVideoPosition = player.getCurrentPosition();
        player.reset();
        player.release();
        player = null;

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (player == null) return;
            player.prepare();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        player.seekTo(currentVideoPosition);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        if (player==null) return;
        player.setDisplay(holder);
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }
}