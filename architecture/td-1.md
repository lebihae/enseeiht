# Multiplexeur

## Interface

```vhdl
entity mux is
    port (e_1, e_2 : in std_logic;
                s   : out std_logic );
end mux;
```

## Les trois vues

### Équation logique

```
s = (!selection) & e_1 + selection & e_2
```

### Circuit

_Les `~truc` sont les noms des signaux, et les `#truc` les noms des portes._

```
selection
|  e_1
|  |  e_2
|  |  |         #u1    ~a
|-------------[ non ]------\       ~b
|  |  |              #u2 [ et ]-----------\
|  |-----------------------/              |          s
|  |  |                             #u4 [ ou ]--------
|--------------------------\       ~c     |
|  |  |              #u3 [ et ]-----------/
|  |  |--------------------/
```

### VHDL

**Signaux = connexions entre les portes**

```vhdl
entity not is
    port (a: in std_logic; s: out std_logic)
end and;
entity and is
    port (a, b: in std_logic; s: out std_logic)
end and;
entity or is
    port (a, b: in std_logic; s: out std_logic)
end or;

architecture structurelle of mux is
component and
    port (a, b: in std_logic; s: out std_logic)
end component;
component or
    port (a, b: in std_logic; s: out std_logic)
end component;
component not 
    port (a: in std_logic; s: out std_logic)
end component;

signal a, b, c : std_logic;

begin

u1 : not port map(selection, a);
u2 : and port map(a, e_1, b);
u3 : and port map(selection, e_2, c);
u4 : or port map(b, c, s);

end structurelle;
```
