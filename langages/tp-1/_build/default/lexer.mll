{
    open Parser
    exception Error of string
}

(* Définitions de macro pour les expressions régulières *)
let blanc = [' ' '\t' '\n']
let digit = ['0'-'9']
let zero_to_255 = (digit | digit digit |'1' digit digit |'2' ['0'-'4'] digit | '2' '5' ['0'-'5'])
let ip = zero_to_255 "." zero_to_255 "." zero_to_255 "." zero_to_255

let ident = ['a'-'z''A'-'Z''0'-'9']+

(* Règles léxicales *)
rule interface = parse
| blanc { interface lexbuf }
| "auto" { AUTO }
| "loopback" { LOOPBACK } 
| "dhcp" { DHCP }
| "iface" { IFACE }
| "inet" { INET }
| "static"  { STATIC }
| "gateway" { GATEWAY }
| "address" { ADDRESS }
| "netmask" { NETMASK}
| ip as ipaddr { IP ipaddr }
| ident as id { IDENT id }
| eof { EOF }
| _
{ raise (Error ("Unexpected char: "^(Lexing.lexeme lexbuf)^" at "^(string_of_int (Lexing.lexeme_start
lexbuf))^"-"^(string_of_int (Lexing.lexeme_end lexbuf)))) }
