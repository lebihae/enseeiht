
module MenhirBasics = struct
  
  exception Error
  
  let _eRR =
    fun _s ->
      raise Error
  
  type token = 
    | STATIC
    | NETMASK
    | LOOPBACK
    | IP of (
# 11 "parser.mly"
       (string)
# 18 "parser.ml"
  )
    | INET
    | IFACE
    | IDENT of (
# 6 "parser.mly"
       (string)
# 25 "parser.ml"
  )
    | GATEWAY
    | EOF
    | DHCP
    | AUTO
    | ADDRESS
  
end

include MenhirBasics

# 1 "parser.mly"
  
    type interface = Auto of string | Iface of string

# 41 "parser.ml"

type ('s, 'r) _menhir_state = 
  | MenhirState00 : ('s, _menhir_box_is) _menhir_state
    (** State 00.
        Stack shape : .
        Start symbol: is. *)

  | MenhirState19 : (('s, _menhir_box_is) _menhir_cell1_i, _menhir_box_is) _menhir_state
    (** State 19.
        Stack shape : i.
        Start symbol: is. *)


and ('s, 'r) _menhir_cell1_i = 
  | MenhirCell1_i of 's * ('s, 'r) _menhir_state * (interface)

and 's _menhir_cell0_IDENT = 
  | MenhirCell0_IDENT of 's * (
# 6 "parser.mly"
       (string)
# 62 "parser.ml"
)

and ('s, 'r) _menhir_cell1_IFACE = 
  | MenhirCell1_IFACE of 's * ('s, 'r) _menhir_state

and _menhir_box_is = 
  | MenhirBox_is of (string list * string list) [@@unboxed]

let _menhir_action_1 =
  fun id ->
    (
# 47 "parser.mly"
                (Auto(id))
# 76 "parser.ml"
     : (interface))

let _menhir_action_2 =
  fun () ->
    (
# 48 "parser.mly"
                (Auto("loopback"))
# 84 "parser.ml"
     : (interface))

let _menhir_action_3 =
  fun id ->
    (
# 49 "parser.mly"
                        (Iface(id))
# 92 "parser.ml"
     : (interface))

let _menhir_action_4 =
  fun accumulateds current ->
    (
# 39 "parser.mly"
                            (
    let (autos, ifaces) = accumulateds in match current with 
    | Auto(eth) -> (eth::autos, ifaces)
    | Iface(eth) -> (autos, eth::ifaces)
    )
# 104 "parser.ml"
     : (string list * string list))

let _menhir_action_5 =
  fun () ->
    (
# 44 "parser.mly"
       (([], []))
# 112 "parser.ml"
     : (string list * string list))

let _menhir_action_6 =
  fun () ->
    (
# 52 "parser.mly"
           (())
# 120 "parser.ml"
     : (unit))

let _menhir_action_7 =
  fun () ->
    (
# 53 "parser.mly"
        (())
# 128 "parser.ml"
     : (unit))

let _menhir_action_8 =
  fun () ->
    (
# 54 "parser.mly"
                                          (())
# 136 "parser.ml"
     : (unit))

let _menhir_print_token : token -> string =
  fun _tok ->
    match _tok with
    | ADDRESS ->
        "ADDRESS"
    | AUTO ->
        "AUTO"
    | DHCP ->
        "DHCP"
    | EOF ->
        "EOF"
    | GATEWAY ->
        "GATEWAY"
    | IDENT _ ->
        "IDENT"
    | IFACE ->
        "IFACE"
    | INET ->
        "INET"
    | IP _ ->
        "IP"
    | LOOPBACK ->
        "LOOPBACK"
    | NETMASK ->
        "NETMASK"
    | STATIC ->
        "STATIC"

let _menhir_fail : unit -> 'a =
  fun () ->
    Printf.eprintf "Internal failure -- please contact the parser generator's developers.\n%!";
    assert false

include struct
  
  [@@@ocaml.warning "-4-37"]
  
  let _menhir_run_18 : type  ttv_stack. ttv_stack -> _ -> _menhir_box_is =
    fun _menhir_stack _v ->
      MenhirBox_is _v
  
  let rec _menhir_goto_is : type  ttv_stack. ttv_stack -> _ -> (ttv_stack, _menhir_box_is) _menhir_state -> _menhir_box_is =
    fun _menhir_stack _v _menhir_s ->
      match _menhir_s with
      | MenhirState19 ->
          _menhir_run_20 _menhir_stack _v
      | MenhirState00 ->
          _menhir_run_18 _menhir_stack _v
  
  and _menhir_run_20 : type  ttv_stack. (ttv_stack, _menhir_box_is) _menhir_cell1_i -> _ -> _menhir_box_is =
    fun _menhir_stack _v ->
      let MenhirCell1_i (_menhir_stack, _menhir_s, current) = _menhir_stack in
      let accumulateds = _v in
      let _v = _menhir_action_4 accumulateds current in
      _menhir_goto_is _menhir_stack _v _menhir_s
  
  let _menhir_run_14 : type  ttv_stack. ttv_stack -> (ttv_stack, _menhir_box_is) _menhir_state -> _menhir_box_is =
    fun _menhir_stack _menhir_s ->
      let _v = _menhir_action_5 () in
      _menhir_goto_is _menhir_stack _v _menhir_s
  
  let rec _menhir_run_01 : type  ttv_stack. ttv_stack -> _ -> _ -> (ttv_stack, _menhir_box_is) _menhir_state -> _menhir_box_is =
    fun _menhir_stack _menhir_lexbuf _menhir_lexer _menhir_s ->
      let _menhir_stack = MenhirCell1_IFACE (_menhir_stack, _menhir_s) in
      let _tok = _menhir_lexer _menhir_lexbuf in
      match (_tok : MenhirBasics.token) with
      | IDENT _v ->
          let _menhir_stack = MenhirCell0_IDENT (_menhir_stack, _v) in
          let _tok = _menhir_lexer _menhir_lexbuf in
          (match (_tok : MenhirBasics.token) with
          | INET ->
              let _tok = _menhir_lexer _menhir_lexbuf in
              (match (_tok : MenhirBasics.token) with
              | STATIC ->
                  let _tok = _menhir_lexer _menhir_lexbuf in
                  (match (_tok : MenhirBasics.token) with
                  | ADDRESS ->
                      let _tok = _menhir_lexer _menhir_lexbuf in
                      (match (_tok : MenhirBasics.token) with
                      | IP _ ->
                          let _tok = _menhir_lexer _menhir_lexbuf in
                          (match (_tok : MenhirBasics.token) with
                          | NETMASK ->
                              let _tok = _menhir_lexer _menhir_lexbuf in
                              (match (_tok : MenhirBasics.token) with
                              | IP _ ->
                                  let _tok = _menhir_lexer _menhir_lexbuf in
                                  (match (_tok : MenhirBasics.token) with
                                  | GATEWAY ->
                                      let _tok = _menhir_lexer _menhir_lexbuf in
                                      (match (_tok : MenhirBasics.token) with
                                      | IP _ ->
                                          let _tok = _menhir_lexer _menhir_lexbuf in
                                          let _ = _menhir_action_8 () in
                                          _menhir_goto_t _menhir_stack _menhir_lexbuf _menhir_lexer _tok
                                      | _ ->
                                          _eRR ())
                                  | _ ->
                                      _eRR ())
                              | _ ->
                                  _eRR ())
                          | _ ->
                              _eRR ())
                      | _ ->
                          _eRR ())
                  | _ ->
                      _eRR ())
              | LOOPBACK ->
                  let _tok = _menhir_lexer _menhir_lexbuf in
                  let _ = _menhir_action_6 () in
                  _menhir_goto_t _menhir_stack _menhir_lexbuf _menhir_lexer _tok
              | DHCP ->
                  let _tok = _menhir_lexer _menhir_lexbuf in
                  let _ = _menhir_action_7 () in
                  _menhir_goto_t _menhir_stack _menhir_lexbuf _menhir_lexer _tok
              | _ ->
                  _eRR ())
          | _ ->
              _eRR ())
      | _ ->
          _eRR ()
  
  and _menhir_goto_t : type  ttv_stack. (ttv_stack, _menhir_box_is) _menhir_cell1_IFACE _menhir_cell0_IDENT -> _ -> _ -> _ -> _menhir_box_is =
    fun _menhir_stack _menhir_lexbuf _menhir_lexer _tok ->
      let MenhirCell0_IDENT (_menhir_stack, id) = _menhir_stack in
      let MenhirCell1_IFACE (_menhir_stack, _menhir_s) = _menhir_stack in
      let _v = _menhir_action_3 id in
      _menhir_goto_i _menhir_stack _menhir_lexbuf _menhir_lexer _v _menhir_s _tok
  
  and _menhir_goto_i : type  ttv_stack. ttv_stack -> _ -> _ -> _ -> (ttv_stack, _menhir_box_is) _menhir_state -> _ -> _menhir_box_is =
    fun _menhir_stack _menhir_lexbuf _menhir_lexer _v _menhir_s _tok ->
      let _menhir_stack = MenhirCell1_i (_menhir_stack, _menhir_s, _v) in
      match (_tok : MenhirBasics.token) with
      | IFACE ->
          _menhir_run_01 _menhir_stack _menhir_lexbuf _menhir_lexer MenhirState19
      | EOF ->
          _menhir_run_14 _menhir_stack MenhirState19
      | AUTO ->
          _menhir_run_15 _menhir_stack _menhir_lexbuf _menhir_lexer MenhirState19
      | _ ->
          _eRR ()
  
  and _menhir_run_15 : type  ttv_stack. ttv_stack -> _ -> _ -> (ttv_stack, _menhir_box_is) _menhir_state -> _menhir_box_is =
    fun _menhir_stack _menhir_lexbuf _menhir_lexer _menhir_s ->
      let _tok = _menhir_lexer _menhir_lexbuf in
      match (_tok : MenhirBasics.token) with
      | LOOPBACK ->
          let _tok = _menhir_lexer _menhir_lexbuf in
          let _v = _menhir_action_2 () in
          _menhir_goto_i _menhir_stack _menhir_lexbuf _menhir_lexer _v _menhir_s _tok
      | IDENT _v ->
          let _tok = _menhir_lexer _menhir_lexbuf in
          let id = _v in
          let _v = _menhir_action_1 id in
          _menhir_goto_i _menhir_stack _menhir_lexbuf _menhir_lexer _v _menhir_s _tok
      | _ ->
          _eRR ()
  
  let _menhir_run_00 : type  ttv_stack. ttv_stack -> _ -> _ -> _menhir_box_is =
    fun _menhir_stack _menhir_lexbuf _menhir_lexer ->
      let _menhir_s = MenhirState00 in
      let _tok = _menhir_lexer _menhir_lexbuf in
      match (_tok : MenhirBasics.token) with
      | IFACE ->
          _menhir_run_01 _menhir_stack _menhir_lexbuf _menhir_lexer _menhir_s
      | EOF ->
          _menhir_run_14 _menhir_stack _menhir_s
      | AUTO ->
          _menhir_run_15 _menhir_stack _menhir_lexbuf _menhir_lexer _menhir_s
      | _ ->
          _eRR ()
  
end

let is =
  fun _menhir_lexer _menhir_lexbuf ->
    let _menhir_stack = () in
    let MenhirBox_is v = _menhir_run_00 _menhir_stack _menhir_lexbuf _menhir_lexer in
    v

# 57 "parser.mly"
  

# 322 "parser.ml"
