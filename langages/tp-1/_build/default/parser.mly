%{
    type interface = Auto of string | Iface of string
%}


%token <string> IDENT
%token AUTO
%token IFACE
%token INET
%token EOF
%token <string> IP  
%token LOOPBACK
%token DHCP 
%token STATIC 
%token GATEWAY 
%token NETMASK 
%token ADDRESS

(* Exercice 2 *)
(* Déclarations du type de l'attribut associé à un non terminal *)
(* Dans un premier temps on ignore cet attribut -> type unit *)
%type <interface> i
%type <unit> t

(* Indication de l'axiom et du type de l'attribut associé à l'axiom *)
(* Dans un premier temps on ignore cet attribut -> type unit *)
%start <string list * string list> is

%%

(*
IS -> I IS
IS -> $

I -> ...
*)

is :
| current=i accumulateds=is {
    let (autos, ifaces) = accumulateds in match current with 
    | Auto(eth) -> (eth::autos, ifaces)
    | Iface(eth) -> (autos, eth::ifaces)
    } 
| EOF  {([], [])}

i:
| AUTO id=IDENT {Auto(id)}
| AUTO LOOPBACK {Auto("loopback")}
| IFACE id=IDENT INET t {Iface(id)}

t:
| LOOPBACK {()}
| DHCP  {()}
| STATIC ADDRESS IP NETMASK IP GATEWAY IP {()}


%%
