let analyse_type_expr = function
| Binaire b lhs rhs -> let ((rhs_value, rhs_type),(lhs_value, lhs_type)) = (analyser_exp lhs, analyser_expr rhs) in
        if EstCompatible rhs_type lhs_type then
                AstType.Binaire info lhs_value rhs_value
        else
                raise E
