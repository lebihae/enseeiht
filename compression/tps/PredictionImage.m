
% TP Codages JPEG et MPEG-2 - 3SN-M - 2022

%--------------------------------------------------------------------------
% Prediction de l'image courante avec l'image de reference et le mouvement
%--------------------------------------------------------------------------
% Ip = PredictionImage(Ir,MVr)
%
% sortie  : Ip = image predictive
%           
% entrees : Ir = image de reference
%           MVr = matrice des vecteurs de déplacements relatifs
%--------------------------------------------------------------------------

function Ip = PredictionImage(Ir,MVr)

    for i = 1:16:size(Ir, 1)
        for j = 1:16:size(Ir, 2)
            Vm = MVr(ceil(i/16), ceil(j/16), :);
            Ip(i:i+15, j:j+15) = Ir(min(i+Vm(1), 1):max(i+15+Vm(1), size(Ir, 1)), min(j+Vm(2), 1):max(j+15+Vm(2), size(Ir,2)));
        end
    end


end
