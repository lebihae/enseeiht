
% TP Codages JPEG et MPEG-2 - 3SN-M - 2022

%--------------------------------------------------------------------------
% Fonction d'estimation du mouvement par "block-matching"
%--------------------------------------------------------------------------
% MVr = EstimationMouvement(Ic,Ir)
%
% sortie  : MVdr = matrice des vecteurs de deplacements relatifs
% 
% entrees : Ic = image courante
%           Ir = image de reference
%--------------------------------------------------------------------------

function MVr = EstimationMouvement(Ic,Ir)
    MVr = zeros([size(Ic) 2]);
    block_i = 0;
    block_j = 0;
    for i = 1:16:size(Ic, 1)
        block_i = block_i+1;
        for j = 1:16:size(Ic, 2)
            block_j = block_j+1;
            MBc = Ic(i:i+15, j:j+15);
            Vp = [i j];
            Vm = CSAMacroBloc(MBc, Vp, Ir);
            MVr(block_i, block_j, :) = Vm;
        end
        block_j = 0;
    end
end

%--------------------------------------------------------------------------
% Fonction de recherche par 'Cross Search Algorithm' :         
%   - Recherche pour un macro-bloc de l'image courante
%--------------------------------------------------------------------------
% Vm = CSAMacroBloc(MBc, Vp, Iref)
%
% sorties : Vm = vecteur de mouvement 
% 
% entr�es : Mbc = macro-bloc dans l'image courante Ic
%           Vp = vecteur de prediction (point de depart du MacroBloc)
%           Iref = image de reference (qui sera conservee dans le GOP)
%--------------------------------------------------------------------------

function Vm = CSAMacroBloc(MB_IC, Vp, IRef)
 coordx = Vp(1):min(Vp(1)+16, size(IRef, 1));
 coordy = Vp(2):min(Vp(2)+16, size(IRef, 2));
 refheight = size(IRef, 1);
refwidth = size(IRef, 2);
 EQM_C = EQMMacrocBlocVoisin(MB_IC(:), IRef, refwidth, refheight, coordx, coordy, "centre");
 EQM_G = EQMMacrocBlocVoisin(MB_IC(:), IRef, refwidth, refheight, coordx, coordy, "gauche");
 EQM_D = EQMMacrocBlocVoisin(MB_IC(:), IRef, refwidth, refheight, coordx, coordy, "droite");
 EQM_H = EQMMacrocBlocVoisin(MB_IC(:), IRef, refwidth, refheight, coordx, coordy, "haut");
    EQM_B = EQMMacrocBlocVoisin(MB_IC(:), IRef, refwidth, refheight, coordx, coordy, "bas");

    [~, idx] = min([EQM_C, EQM_G, EQM_D, EQM_H, EQM_B]);
    if idx == 1
        Vm = [0 0];
    elseif idx == 2
        Vm = [0 -1];
    elseif idx == 3
        Vm = [0 1];
    elseif idx == 4
        Vm = [-1 0];
    elseif idx == 5
        Vm = [1 0];
    end

end

%--------------------------------------------------------------------------
% Fonction de calcul de l'EQM avec differents voisins 
% dans l'image de reference
%--------------------------------------------------------------------------
% EQM = EQMMacrocBlocVoisin(MB_IC_V,IRef,size_x_Ref,size_y_Ref,coordx,coordy,voisin)
%
% sortie  : EQM = erreur quadratique moyenne entre macro-blocs
% 
% entrees : MB_IC_V = macro-bloc dans l'image courante (vectorise)
%           Ir = Image de reference
%           size_x_Ir = nombre de lignes de Ir (pour effets de bords)
%           size_y_Ir = nombre de colonnes de Ir (pour effets de bords)
%           coordx = les 16 coordonnees du bloc suivant x
%           coordy = les 16 coordonnees du bloc suivant y
%           voisin = choix du voisin pour decaler le macro-bloc dans Ir
%                    ('haut', 'gauche', 'centre', 'droite', bas', ...)
%--------------------------------------------------------------------------

function EQM = EQMMacrocBlocVoisin(MB_IC_V,Ir,size_x_Ir,size_y_Ir,coordx,coordy,voisin)
    if voisin == "centre"
        next_block_pixels = Ir(coordx, coordy);
    elseif voisin == "haut"
        next_block_pixels = Ir(...
            coordx, ...
            min(coordy(1) + 1, size_y_Ir):min(coordy(end)  +1, size_y_Ir)...
        );
    elseif voisin == "bas"
        next_block_pixels = Ir(...
            coordx, ...
            max(coordy(1) - 1, 1):min(coordy(1) + 1, size_y_Ir)...
        );
    elseif voisin == "gauche"
        next_block_pixels = Ir(...
            max(coordx(1) - 1, 1):min(coordx(end) - 1, size_x_Ir),...
            coordy...
        );
    elseif voisin == "droite"
        next_block_pixels = Ir(...
            max(coordx(1) + 1, 1):min(coordx(end) - 1, size_x_Ir),...
            coordy...
        );
    end

    next_block_pixels = next_block_pixels(:);

    EQM = 0;
    for i = 1:min(16, length(next_block_pixels))
        EQM = EQM + (MB_IC_V(i) - next_block_pixels(i))^2;
    end
end
