
% TP Codages JPEG et MPEG-2 - 3SN-M - 2022

%--------------------------------------------------------------------------
% Fonction de calcul d'entropie binaire
%--------------------------------------------------------------------------
% [poids, H] = CodageEntropique(V_coeff)
%
% sorties : poids = poids du vecteur de donnees encode (en ko)
%           H = entropie de la matrice (en bits/pixel)
%
% entree  : V_coeff = vecteur contenant les symboles dont on souhaite
%                     calculer l'entropie (ex : l'image vectorisee)
%--------------------------------------------------------------------------

function [poids, H] = CodageEntropique(V_coeff)
    % EOB = 1000;
% Remove EOB from V_coeff
% V_coeff = V_coeff(V_coeff ~= EOB);
lowest_coeff = double(min(V_coeff))
highest_coeff = double(max(V_coeff))


% tablo des frequences de chacuns des coeffs, de lo a hi
frequencies = zeros(1, highest_coeff - lowest_coeff + 1);
for i = 1:length(V_coeff)
    frequencies(V_coeff(i) - lowest_coeff + 1) = frequencies(V_coeff(i) - lowest_coeff + 1) + 1;
end


H = -sum(...
    frequencies(frequencies > 0) / length(V_coeff) .* log2(frequencies(frequencies > 0) / length(V_coeff))...
    );

poids = H * length(V_coeff) / 8 / 1024;
end
