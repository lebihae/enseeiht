# Rhetorical devices

- anaphora
- sarcasm
- refute idea in advance
- hyperobola
- metaphor
- reference
- anastrophe
- syllogism
- rhetorical Q
- concession 
- rule of 3
- quotes
- examples
- alliteration 


## Examples in "Hope is the thing with feathers" by Emily Dickinson

- "and sore must be ther storm" allit in 'o'
- asson in 's'
- personification of hope
- rule of 3 -> 3 verses
- repetition of "and" @ start 


Hope is the thing with feathers
----                   --------
That perches in the soul,
                    ----


THB     this house believes
THW     this house would
THR     this house regrets

THBT we should say "pain au chocolatine" 
THW replace SHS with TVn7, net7, CAn7, Photo7 and 7Fault.

