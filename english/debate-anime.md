# アニメ > series ????

## For

| /                      | n7beatsのviceprez | ?   | le fantôme |
| ---------------------- | ---------------- | --- | ---------- |
| Arguments              | 7                | 4   |            |
| Presentation           | 7                | 6   |            |
| Teamwork & strategy    | +1               |     |            |
| Final individual grade |                  |     |            |

## Against

| /                      | あにちゃん | clément?? | axe l |
| ---------------------- | ----- | --------- | ----- |
| Arguments              | 6     | 5         | 6     |
| Presentation           | 7     | 6.5       | 7.5   |
| Teamwork & strategy    |       | +0.5 +0.5 |       |
| Final individual grade |       |           |       |
