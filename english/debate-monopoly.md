# Monopoly > * ????



## For

| /                      | P'tite Balise? | Coco                | Benj        |
| ---------------------- | -------------- | ------------------- | ----------- |
| Arguments              | 5/10           | (+.5)  8.5/10       | 5.5/10      |
| Presentation           | 9/10           | 8/10                | 8/10        |
| Teamwork & strategy    |                | (+1.5)  (+.5) (+.5) | (+.5) (+.5) |
| Final individual grade | 12.75          | 16.75               | 13          |





## Against

| /                      | Romain | Rami  | Michel     |
| ---------------------- | ------ | ----- | ---------- |
| Arguments              | 5.5/10 | 5/10  | 7/10       |
| Presentation           | 7/10   | 4/10  | 7.5/10     |
| Teamwork & strategy    |        | (~.5) | (+2) (+.5) |
| Final individual grade | 11.75  | 9.75  | 14.75      |


