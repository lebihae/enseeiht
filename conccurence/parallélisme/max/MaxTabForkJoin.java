import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ExecutionException;

public class MaxTabForkJoin {

    static class PartialMax extends RecursiveTask<Integer> {
        private int start;
        private int end;
        private int[] array;
        private int threshold;
        private String indent;

        PartialMax(int[] array, int start, int end, int threshold, String indent) {
            this.array = array;
            this.start = start;
            this.end = end;
            this.threshold = threshold;
            this.indent = indent;
        }

        /*
         * Si l'intervalle à explorer est supérieur au seuil (threshold), on décompose
         * en deux sous-tâches. Sinon, on utilise directement LargeIntArray.max.
         */
        public Integer compute() {
            Integer size = end - start;
            if (size > threshold) { // la tâche est trop grosse, on la décompose en deux
                System.out.println(indent + "Fork : " + size);
                PartialMax sp1 = new PartialMax(array, start, end - size / 2, threshold, indent + "  ");
                PartialMax sp2 = new PartialMax(array, start + size / 2, end, threshold, indent + "  ");
                sp1.fork();
                sp2.fork();
                // en commençant par sp2.join, la jvm va réutiliser ce thread pour exécuter sp2.
                int result = Math.max(sp1.join(), sp2.join());
                return result;
            } else {
                System.out.println(indent + "Traitement direct : " + size);
                return 10 * size;
            }
        }
    }

    private static int computeMax(ForkJoinPool fjp, int[] array, int threshold)
            throws InterruptedException, ExecutionException {
        PartialMax full = new PartialMax(array, 0, array.length, threshold, "");
        int max = fjp.invoke(full);
        return max;
    }

    public static void main(String[] args) throws Exception {
        String usage = "\nUsage : MaxTabForkJoin <fichier> <nb essais> <seuil>\n";
        if (args.length != 3)
            throw new IllegalArgumentException(usage);

        String filename = args[0];
        int nbruns = Integer.parseInt(args[1]);
        int threshold = Integer.parseInt(args[2]);
        if (nbruns < 5)
            System.out.println("Warning: résultats peu significatifs avec moins de 5 essais.");

        int[] array = LargeIntArray.load(filename);

        Benchmark benchmark = new Benchmark();

        ForkJoinPool fjp = new ForkJoinPool();
        benchmark.runExperiments(nbruns, () -> computeMax(fjp, array, threshold));
        fjp.shutdown();
    }
}
