#include "ocv_utils.hpp"
#include <set>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>
#include "kmeans.h"

using namespace cv;
using namespace std;
// scores: true_positive, false_positive, true_negative, false_negative;
void update_scores(float ground_truth_pixel, float actual_pixel, int scores[4])
{
    if (ground_truth_pixel == 0)
    {
        if (actual_pixel == 0)
        {
            scores[1]++;
        }
        else
        {
            scores[2]++;
        }
    }
    else
    {
        if (actual_pixel == 0)
        {
            scores[3]++;
        }
        else
        {
            scores[0]++;
        }
    }
}

void print_scores(cv::Mat &reshaped_result, cv::Mat &reference)
{
    int scores[] = {0, 0, 0, 0};

    for (int i = 0; i < reshaped_result.rows; i++)
    {
        for (int j = 0; j < reshaped_result.cols; j++)
        {
            update_scores(reference.at<float>(i, j), reshaped_result.at<float>(i, j), scores);
        }
    }

    float true_positive = (float)scores[0] / reference.total();
    float false_positive = (float)scores[1] / reference.total();
    float true_negative = (float)scores[2] / reference.total();
    float false_negative = (float)scores[3] / reference.total();

    cout << "TP = " << true_positive << "; TN = " << true_negative << "; FP = " << false_positive << "; FN = " << false_negative << endl;

    float precision = true_positive / (true_positive + false_positive);
    float sensibility = true_positive / (true_positive + false_negative);
    float similarity = (true_positive + true_negative) / (true_positive + true_negative + false_positive + false_negative);

    // pour texture8
    // Precision = 92.4071%; Sensibility = 82.0803%; Similarity = 81.9107%;
    cout << "Precision = " << precision * 100 << "%; Sensibility = " << sensibility * 100 << "%; Similarity = " << similarity * 100 << "%;" << endl;
}

void print_unique_values(Mat m)
{
    set<float> seen_values;
    for (size_t i = 0; i < m.total(); i++)
    {
        auto value = m.at<float>(1, i);
        seen_values.insert(value);
    }

    for (auto v : seen_values)
    {
        cout << v << ", ";
    }

    cout << endl;
}

void printHelp(const string &progName)
{
    cout << "Usage:\n\t " << progName << " <image_file> <K_num_of_clusters> [<image_ground_truth>]" << endl;
}

void kmeans_iteration(InputArray data, int K, OutputArray &bestLabels, Mat centroids)
{
    cout << "Starting bestLabels step" << endl;
    cv::sort(centroids, centroids, SORT_EVERY_COLUMN + SORT_ASCENDING);
    for (int i = 0; i < data.rows(); i++)
    {
        Mat point = data.getMat().row(i);

        Mat distances_to_centroids = (repeat(point, K, 1) - centroids);
        distances_to_centroids = distances_to_centroids.mul(distances_to_centroids);

        if (i == 512 * 200 + 256)
            cout << "(one point at center)\tDistances to means: " << distances_to_centroids << endl;

        auto closest_centroid_index = 0;
        for (int centroid_index = 0; centroid_index < centroids.rows; centroid_index++)
        {
            auto distance_to_this_centroid = mean(distances_to_centroids.row(centroid_index));
            auto distance_to_closest_yet = mean(distances_to_centroids.row(closest_centroid_index));
            if (distance_to_this_centroid[0] < distance_to_closest_yet[0])
            {
                closest_centroid_index = centroid_index;
            }
        }
        bestLabels.getMat().at<int>(i) = closest_centroid_index;
        cout << "labels:";
        print_unique_values(bestLabels.getMat());
    }

    for (int i = 0; i < K; i++)
    {
        cout << "Updating mean for cluster " << i << endl;

        Mat mask = repeat(bestLabels.getMat() == i, 1, data.cols());
        Mat points_of_cluster = Mat::zeros(data.size(), CV_32F);
        data.copyTo(points_of_cluster, mask);
        if (countNonZero(mask) == 0)
        {
            cout << "No points for cluster " << i << endl;
            continue;
        }
        Mat new_centroid = Mat::zeros(1, centroids.cols, CV_32F);
        reduce(points_of_cluster, new_centroid, REDUCE_SUM, 1);
        for (int centroid_coord_index = 0; centroid_coord_index < centroids.cols; centroid_coord_index++)
        {
            centroids.at<float>(i, centroid_coord_index) = new_centroid.at<float>(centroid_coord_index) / countNonZero(mask);
        }
        cout << "computed mean(" << i << ") = " << new_centroid.col(i) << " from " << countNonZero(mask) << " points" << endl;
    }
    cout << "means = " << centroids << endl;
}

void my_kmeans(InputArray data, int K, Mat &bestLabels, TermCriteria criteria, int attempts, int flags, OutputArray centers, Mat groundTruth)
{
    Mat denormalized_data = data.getMat() / 255;
    cout << "Starting kmeans on matrix of size " << data.size() << " with " << K << " clusters" << endl;
    bestLabels = Mat::zeros(data.rows(), 1, CV_32S);
    Mat centroids = Mat::zeros(K, 3, CV_32F);
    double evolution = criteria.epsilon * 10;

    printf("Initializing centroids with random values\n");
    for (int i = 0; i < K; i++)
    {
        cout << "choosing random mean for cluster " << i << endl;
        // Select a random point from data
        int random_index = rand() % data.total();
        cout << "Random index: " << random_index << endl;
        centroids.at<float>(i) = denormalized_data.at<float>(random_index);
    }

    cout << "centroids: " << centroids << endl;

    cout << "Starting kmeans iterations" << endl;
    int iterations_count = 0;
    while (iterations_count < criteria.maxCount && evolution > criteria.epsilon)
    {
        cout << endl
             << endl;
        Mat previous_centroids = centroids.clone();
        kmeans_iteration(denormalized_data, K, bestLabels, centroids);
        cout << "means = " << centroids << endl;
        cout << "Finished bestLabels step" << endl;
        cout << "bestLabels has size " << bestLabels.size() << endl;

        evolution = norm(centroids - previous_centroids, NORM_L2);
        cout << "evolution = " << evolution << endl;

        process_result(bestLabels, K, groundTruth);
        iterations_count++;
    }
    cout << endl
         << endl;
}

void process_result(cv::Mat &bestLabels, int K, cv::Mat &groundTruth)
{
    Mat denormalized_result;
    denormalized_result = bestLabels * 255 / (K - 1);

    cout << "result | uniq = ";
    print_unique_values(bestLabels);
    cout << "result | normalize | uniq = ";
    print_unique_values(denormalized_result);

    cout << groundTruth.size() << endl;
    denormalized_result = denormalized_result.reshape(1, groundTruth.rows);
    PRINT_MAT_INFO(denormalized_result);
    imwrite("classif_result.png", denormalized_result);

    Mat reshaped_result = bestLabels.reshape(1, groundTruth.rows);
    print_scores(reshaped_result, groundTruth);
}
int main(int argc, char **argv)
{
    if (argc != 3 && argc != 4 && argc != 5)
    {
        cout << " Incorrect number of arguments." << endl;
        printHelp(string(argv[0]));
        return EXIT_FAILURE;
    }

    const auto imageFilename = string(argv[1]);
    const string groundTruthFilename = (argc >= 4) ? string(argv[3]) : string();
    const int k = stoi(argv[2]);
    const bool mine = argc == 5 && string(argv[4]) == "--mine";

    // just for debugging
    {
        cout << " Program called with the following arguments:" << endl;
        cout << " \timage file: " << imageFilename << endl;
        cout << " \tk: " << k << endl;
        if (!groundTruthFilename.empty())
            cout << " \tground truth segmentation: " << groundTruthFilename << endl;
        cout << " \tmine: " << mine << endl;
    }

    // load the color image to process from file
    Mat m;
    m = imread(imageFilename, IMREAD_COLOR);
    // for debugging use the macro PRINT_MAT_INFO to print the info about the matrix, like size and type
    PRINT_MAT_INFO(m);

    // 1) in order to call kmeans we need to first convert the image into floats (CV_32F)
    // see the method Mat.convertTo()
    Mat m2;
    m.convertTo(m2, CV_32F);

    // 2) kmeans asks for a mono-dimensional list of "points". Our "points" are the pixels of the image that can be seen as 3D points
    // where each coordinate is one of the color channel (e.g. R, G, B). But they are organized as a 2D table, we need
    // to re-arrange them into a single vector.
    // see the method Mat.reshape(), it is similar to matlab's reshape
    m2 = m2.reshape(1, m2.total());
    // m2.reshape(1);
    PRINT_MAT_INFO(m2);

    // now we can call kmeans(...)
    Mat result;

    if (groundTruthFilename == "")
    {
        return EXIT_SUCCESS;
    }

    Mat reference = imread(groundTruthFilename, IMREAD_GRAYSCALE);
    reference.convertTo(reference, CV_32SC1);

    auto criteria = TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 1000, 0.0001);

    if (mine)
    {
        cout << "starting custom kmeans" << endl;
        my_kmeans(m2, k, result, criteria, 1, KMEANS_RANDOM_CENTERS, noArray(), reference);
    }
    else
    {
        kmeans(m2, k, result, criteria, 1, KMEANS_RANDOM_CENTERS);
        process_result(result, k, reference);
    }

    return EXIT_SUCCESS;
}
