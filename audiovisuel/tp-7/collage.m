function u = collage(r,s,interieur)

r = double(r);
s = double(s);

nb_pixels = length(s);
nb_lignes = size(s,1);
e = ones(nb_pixels,1);

Dx = spdiags([-e e],[0 nb_lignes],nb_pixels,nb_pixels);
Dx(end-nb_lignes+1:end,:) = 0;

Dy = spdiags([-e e],[0 1],nb_pixels,nb_pixels);
Dy(nb_lignes:nb_lignes:end,:) = 0;

A =  -Dx' * Dx - Dy' * Dy;

indices_bord_r = zeros(size(r));
indices_bord_r(1, :) = 1;
indices_bord_r(end, :) = 1;
indices_bord_r(:, 1) = 1;
indices_bord_r(:, end) = 1;

n_bord_r = length(find(indices_bord_r));
n_r = numel(r);

A(indices_bord_r, :) = sparse(1:n_bord_r, indices_bord_r, ones(n_bord_r, 1), n_bord_r, n_r);

% Calcul de l'imagette resultat u, canal par canal :
u = r;
for k = 1:size(r,3)
	u_k = u(:,:,k);
	r_k = r(:,:,k);
	s_k = s(:,:,k);

	pixels_r = reshape(r_k, 1, nb_pixels);
	
	
	g = zeros(size(r));
	D_x = r_k(2:end-1, :) - r_k(1:end-2, :);
	D_y = r_k(:, 2:end-1) - r_k(:, 1:end-2);
	g = [D_x * r , D_y * r];
	
	% u_k(interieur) = s_k(interieur);
	% u(:,:,k) = u_k;
end
