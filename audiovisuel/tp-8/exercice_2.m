clear;
close all;

lambda = 10
epsilon = 0.01
max_iterations = 20

I = double(imread("./healthy_1.png"));
[width, height] = size(I)
N = width*height;
u = reshape(I, [N, 1]);
k = 0;

I_N = speye(N);
e = ones(N,1);
Dx = spdiags([-e e],[0 width],N,N);
Dx(end-width+1:end,:) = 0;
Dy = spdiags([-e e],[0 1],N,N);
Dy(width:width:end,:) = 0;

u_bar = u;

while k < max_iterations
    diagonal_values = 1 ./ sqrt(gradient(u_bar).^2 + epsilon);
    W = spdiags([diagonal_values diagonal_values], [0 N], N, N);
    A = I_N - lambda * (-Dx' * W * Dx - Dy' * W * Dy);
    
    u_bar = A \ u;
    
    k = k +1
end

I_bar = reshape(u_bar, [width height]);

% Mise en place de la figure pour affichage :
taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);
figure('Name','Decomposition par modification du spectre','Position',[0.2*L,0,0.8*L,H]);

% Affichage des images u, u_filtre et u-u_filtre :
subplot(2,3,1);
affichage(I,'$x$','$y$','Image');
subplot(2,3,2);
affichage(I_bar,'$x$','$y$','Image filtree');
subplot(2,3,3);
affichage(I-I_bar,'$x$','$y$','Image complementaire');
