function optimal_parameters = moindres_carres(points)
% on résout sous forme matricielle alpha x^2 + beta x y + gamma y^2 + delta x + eps y + phi = 0
% pour tout les (x, y) de training
% et on ajoute la contrainte alpha + gamma = 1 (matriciellement: [1 0 1 0 0 0] = 1)
[~, points_count] = size(points);
%                  x^2                      xy                         y^2             x            y               1        
systeme = [ points(1, :)'.^2  points(1, :)' .* points(2, :)'  points(2, :)'.^2  points(1, :)'  points(2, :)'  ones(points_count, 1) ];
% alpha + gamma = 1 (matriciellement: [1 0 1 0 0 0] = 1)
systeme = [ systeme ; 1 0 1 0 0 0 ];
% ... = [0; 0; ....; 0; 1 ] (le 1 vient du alpha + gamma = 1)
second_membre = [ zeros(points_count, 1) ; 1 ];

optimal_parameters = systeme\second_membre;
end

