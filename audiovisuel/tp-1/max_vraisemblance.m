% [semi_large_axis, excentricity, center_x, center_y, semi_large_axis_angle]
function best_parameters = max_vraisemblance(points, parameters)
    [parameters_set_size, ~] = size(parameters);
    offsets = zeros(parameters_set_size, 1);
    for i = 1:parameters_set_size
        offsets(i) = sum(calcul_r(points, parameters(i, :)).^2);
    end
    [~, best_args_row] = min(offsets);
    best_parameters = parameters(best_args_row, :);
end