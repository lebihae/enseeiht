function p = probabilites_EM(training_points, estimated_parameters, ratio_1, ratio_2, sigma)
score = @(ellipse_index) 0.5 / sigma * exp(-(calcul_r(training_points, estimated_parameters(ellipse_index, :))).^2  / (2*sigma^2));

p = [ score(1)' score(2)' ]';
end
