% [1(semi_large_axis, excentricity, center_x, center_y, semi_large_axis_angle), 2(semi_large_axis, excentricity, center_x, center_y, semi_large_axis_angle)]
function best_parameters = max_vraisemblance_2(points, parameters, sigma)
[parameters_set_size, ~, ~] = size(parameters);
offsets = zeros(parameters_set_size, 1);

for i = 1:parameters_set_size
    first_ellipse_parameters = parameters(i, 1,:);
    second_ellipse_parameters = parameters(i, 2, :);
    
    score = @(r) 0.5/sigma  * exp( - r.^2 / ((2 * sigma^2)) );
    
    offsets(i) = sum(log( ...
        score(calcul_r(points, first_ellipse_parameters)) + score(calcul_r(points, second_ellipse_parameters))...
        ));
end

[~, best_args_row] = max(offsets);
best_parameters = parameters(best_args_row, :);
end
