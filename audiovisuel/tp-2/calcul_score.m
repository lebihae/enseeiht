function score = calcul_score(predicted, real)
score = 0;
class_permutations = perms(unique(predicted));

for perm = class_permutations'
    permuted = predicted;
    for i = 1:length(perm)
        permuted(predicted == i) = perm(i);
    end
    
    new_score = sum(permuted == real) / length(predicted);
    score = max(score, new_score);
end
end
