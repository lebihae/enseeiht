function x = moyenne_3D(I)

% Conversion en flottants :
I = single(I);

% Calcul des couleurs normalisees :
somme_canaux = max(1,sum(I,3));
r = I(:,:,1)./somme_canaux;
v = I(:,:,2)./somme_canaux;

% Calcul du pourtour
disk_radius = min(size(I)) / 4;
disk_indices = r(:, 1).^2 + r(:, 2).^2 <= disk_radius.^2;

center_mean_red = mean(r(disk_indices));
outside_mean_red = mean(r(~disk_indices));

% Calcul des couleurs moydeeennes :
red_center_outside_delta = outside_mean_red - center_mean_red;
r_barre = mean(r(:));
v_barre = mean(v(:));
x = [red_center_outside_delta r_barre v_barre];
