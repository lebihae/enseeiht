clear;
close all;

img = imread("Donnees/piments.png");

[width, height, ~] = size(img);

reds = img(:, :, 1);
greens = img(:, :, 2);
blues = img(:, :, 3);
line_numbers = repmat((1:height)', 1, width);
col_numbers = repmat(1:width, height, 1);
