function [D,A] = nmf(S, D_0, A_0, nb_iters)
    A = A_0(:, :);
    D = D_0(:, :);

    for i = 1:nb_iters
       A = A .* (D' * S) ./ (D' * D * A);
       D = D .* (S * A') ./ (D * A * A');
    end

    for k = 1:size(D, 2)
        D(:, k) = D(:, k) ./ max(D(:, k))
    end
