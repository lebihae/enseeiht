function [F_h, F_p] = HPSS(y)
    F_h = medfilt2(y, [1 17]);
    F_p = medfilt2(y, [17 1]);
