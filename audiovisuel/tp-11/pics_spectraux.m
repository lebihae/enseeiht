function [pics_t, pics_f] = pics_spectraux(S, eta_t, eta_f, epsilon)
element = ones(eta_t, eta_f);
element(eta_t / 2, eta_f / 2) = 0;

[pics_f, pics_t] = find( S > max(imdilate(S, element), epsilon));
pics_t = pics_t';
pics_f = pics_f';
