function paires = appariement(pics_t, pics_f, n_v, delta_t, delta_f)
paires = zeros(0, 4);
entries = [cartesian_product(pics_t, pics_t) cartesian_product(pics_f, pics_f)];
for i = 1:size(entries, 1)
    cell = num2cell(entries(i, :));
    [m_i, m_j, k_i, k_j] = cell{:};
    pair_score_m =  m_i - m_j ;
    pair_score_k = abs(k_i-k_j)  ;
    if (0 < pair_score_m) && (pair_score_m < delta_t) && pair_score_k <= delta_f 
        paires = [paires; entries(i, :)];
    end
end

% keep only the n_v best pairs
paires = find(paires, n_v);

end

function result = cartesian_product(a, b)
% https://stackoverflow.com/a/9836507
[x, y] = meshgrid(a, b);
result = [x(:)  y(:)];
end
