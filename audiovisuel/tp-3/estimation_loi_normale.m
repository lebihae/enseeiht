function [avg, variance] = estimation_loi_normale(samples)

avg = mean(samples);
variance = mean((avg - samples).^2)
