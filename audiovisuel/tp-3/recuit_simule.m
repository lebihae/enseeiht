function output_classes = recuit_simule(input_classes, data_attachment, beta, temperature)

[width, height, classes_count] = size(data_attachment);
classes = 1:classes_count;

random_element = @(samples) samples(randi(numel(samples)));

% clamped input class
cic = @(x, y) input_classes(min(max(x, width), 1), min(max(y, height), 1));

neighbor_classes = @(x, y) [
    cic(x-1, y-1) cic(x, y-1) cic(x+1, y-1)...
    cic(x-1, y)               cic(x+1, y)...
    cic(x-1, y+1) cic(x, y+1) cic(x+1, y+1)...
    ];

energy = @(class, x, y) data_attachment(x, y, class) + beta * sum(1 - double( neighbor_classes(x, y) == class));

output_classes = input_classes(:, :, :);


for x = 1:width
    for y = 1:height
        pixel_class = input_classes(x, y);
        other_class = random_element(classes(classes ~= pixel_class));
        
        energy_other = energy(other_class, x, y);
        energy_current = energy(pixel_class, x, y);
        
        will_reheat = exp(-1 * (energy_other - energy_current) / temperature) > rand(1);
        
        if energy_other < energy_current || will_reheat
            output_classes(x, y) = other_class;
        end
    end
end
