function attache = attache_aux_donnees(image, means, variances)

[width, height] = size(image);
attache_of_class = @(mean, variance) 1/2 * ( log(variance) + (image - mean).^2 ./ variance );

attache = zeros(width, height, length(means));
for k = 1:length(means)
    attache(:, :, k) = attache_of_class(means(k), variances(k));
end
