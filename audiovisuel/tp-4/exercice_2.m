clear;
close all;

taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);


% Parametres :
R = 7;					% Rayon des disques
nb_points_affichage_disque = 30;
increment_angulaire = 2*pi/nb_points_affichage_disque;
theta = 0:increment_angulaire:2*pi;
rose = [253 108 158]/255;
q_max = 200;
nb_affichages = 1000;
pas_entre_affichages = floor(q_max/nb_affichages);
temps_pause = 0.0001;

% Paramètres du recuit simulé (et du calcul de l'énergie)
beta = 1
lambda_0 = 100
alpha = 0.99
T_0  =0.1

historique_energies = []

% Lecture et affichage de l'image :
I = imread('colonie.png');
I = rgb2ycbcr(I);
I = double(I(:,:,1));
[nb_lignes,nb_colonnes] = size(I);
figure('Name',['Detection de flamants roses'],'Position',[0,0,L,0.58*H]);

liste_q = 0;

% Affichage de la configuration initiale :
subplot(1,2,1);
imagesc(I);
axis image;
axis off;
colormap gray;
hold on;
pause(temps_pause);

% Courbe d'evolution du niveau de gris moyen :

lambda = lambda_0;

disques = []

% Recherche de la configuration optimale :
T = T_0
for q = 1:q_max
	q
	nombre_nouveaux_disques = poissrnd(lambda, 1);
	for i = 1:nombre_nouveaux_disques
		disques(i, :) = [nb_colonnes*rand nb_lignes*rand];
	end

	disques

	% tri des disques par leurs énergies
	calc_energies = @(disques) energies_disques(I, disques, R);
	[~, indices_tries] = sort(calc_energies(disques));
	indices_tries
	disques = disques(indices_tries, :);
	disp('feur')


	for i = 1:length(disques)
		size(disques)
		disque = disques(i, :);
		energie_totale = sum(calc_energies(disques));
		energie_autres = energie_totale - energie(I, disque, R);
		probabilite_mort = lambda / (lambda + exp((energie_autres - energie_totale) / T));
		if rand < probabilite_mort
			disques(i) = [];
		end
	end


	% if nouveaux_disques
		
	% Si le disque propose est "meilleur", mises a jour :
	% if true 
	% 	c(i,:) = c_alea;
	% 	I_moyen(i) = I_moyen_nouv;
		
	% 	hold off;
	% 	subplot(1,2,1);
	% 	imagesc(I);
	% 	axis image;
	% 	axis off;
	% 	colormap gray;
	% 	hold on;
	% 	for j = 1:N
	% 		x_affich = c(j,1)+R*cos(theta);
	% 		y_affich = c(j,2)+R*sin(theta);
	% 		indices = find(x_affich>0 & x_affich<nb_colonnes & y_affich>0 & y_affich<nb_lignes);
	% 		plot(x_affich(indices),y_affich(indices),'Color',rose,'LineWidth',3);
	% 	end
	% 	pause(temps_pause);
	% end
	
	% Courbe d'evolution du niveau de gris moyen :
	if rem(q,pas_entre_affichages)==0
		liste_q = [liste_q q];
		historique_energies = [historique_energies calc_energies(disques)];
		subplot(1,2,2);
		plot(liste_q,historique_energies,'.-','Color',rose,'LineWidth',3);
		axis([0 q_max -400 0]);
		set(gca,'FontSize',20);
		xlabel('Nombre d''iterations','FontSize',20);
		ylabel('Niveau de gris moyen','FontSize',20);
	end
end


function energies = energies_disques(I, disques, R)
	disp('sizeof disks for energy ')
	size(disques)
	energies = [];
	for i = 1:length(disques)
		energies = [energies energie(I, disques(i,:), R)];
	end
end

function energie = energie(I, disque, R) 
	S = 140;
	gamma_ = 5;
	energie = 1 - 2/(1 + exp(-gamma_ * (calcul_I_moyen(I, disque, R)/S - 1)));
end
