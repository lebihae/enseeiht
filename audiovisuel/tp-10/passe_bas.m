function out = passe_bas(in, frequencies, cutoff)
    out = in;
    out(frequencies > cutoff, :) = 0;
