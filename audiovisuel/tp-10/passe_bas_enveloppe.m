function out = passe_bas_enveloppe(in, frequencies, starting_cutoff, ending_cutoff, duration)
    out = in;
    cutoff_step_size = (ending_cutoff - starting_cutoff) / duration;
    max_avg_amplitude = max(mean(abs(in), 1));
    min_avg_amplitude = min(mean(abs(in), 1));
    [min_avg_amplitude, max_avg_amplitude]

    j = 0;
    epsilon = 1;
    for i = 1:size(in, 2)
        avg_amplitude = mean(abs(in(:, i)))
        if avg_amplitude > max_avg_amplitude - epsilon
            j = 0;
        else
            j = j +1;
        end
        cutoff = starting_cutoff + cutoff_step_size * j;
        out(frequencies > cutoff, i) = 0;
    end
