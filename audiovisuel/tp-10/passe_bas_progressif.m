function out = passe_bas_progressif(in, frequencies, starting_cutoff, ending_cutoff, duration)
    out = in;
    cutoff_step_size = (ending_cutoff - starting_cutoff) / duration;
    for i = 1:size(in, 2)
        cutoff = starting_cutoff + cutoff_step_size * i;
        out(frequencies > cutoff, i) = 0;
    end
