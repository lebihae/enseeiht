function u_kp1 = inpainting(b,u_k,lambda,Dx,Dy,epsilon, domaine_du_defaut)
    pixel_count = length(u_k);

    gradient_values = mean(1 ./ sqrt(gradient(u_k).^2 + epsilon), 2);
    Wk = spdiags([gradient_values gradient_values], [0 pixel_count], pixel_count, pixel_count);

    gradient_values = ones(pixel_count, 1);
    gradient_values(domaine_du_defaut) = 0;
    W_defaut = spdiags([gradient_values gradient_values], [0 pixel_count], pixel_count, pixel_count);

    u_kp1 = (W_defaut - lambda * ( -Dx' * Wk * Dx - Dy' * Wk * Dy)) \ (W_defaut* b);
