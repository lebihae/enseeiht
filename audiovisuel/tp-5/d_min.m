function [existe_q,bornes_V_p,bornes_V_q_chapeau] = d_min(i_p,j_p,u_k,D,t,T)

search_window_size = 2*T+1
[width, height] = size(u_k);
clip_x = @(x) min(max(x, 1), width);
clip_y = @(y) min(max(y, 1), height);

search_window = u_k(...
    clip_x(i_p - search_window_size) : clip_x(i_p + search_window_size),...
    clip_y(j_p - search_window_size) : clip_y(j_p + search_window_size)...
    );

patch_size = 2*t+1

dissemblance = @(p, q) 1 / length(patch_indices(p, patch_size, D)) * sum(norm( u(patch_indices(p, patch_size, D)) - u(patch_indices(q, patch_size, D)) ).^2);

q_chapeau = [0, 0];

target_center = u_k(i_p, j_p);
min_dissemblance = +Inf;
for center = search_window
    if isempty(patch_indices(center, patch_size, D))
        continue
    end
    
    this_dissemblance = dissemblance(target_center, center);
    if this_dissemblance < min_dissemblance
        q_chapeau = center;
        min_dissemblance = this_dissemblance;
    end
end

existe_q = min_dissemblance < +Inf;
bornes_V_p = patch_indices(target_center, patch_size, D);
bornes_V_q_chapeau = patch_indices(q_chapeau, patch_size, D);
end

function indices = patch_indices(center, patch_size, D)
[width, height] = size(D);
clip_x = @(x) min(max(x, 1), width);
clip_y = @(y) min(max(y, 1), height);
center_x = center(1);
center_y = center(2);

indices = [
    clip_x(center_x - patch_size) : clip_x(center_x + patch_size);
    clip_y(center_y - patch_size) : clip_y(center_y + patch_size)
];

indices(D ~= 0) = []
end
