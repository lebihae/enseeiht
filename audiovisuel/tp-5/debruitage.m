function u_kp1 = debruitage(b,u_k,lambda,Dx,Dy,epsilon)
    gradient_values = mean(1 ./ sqrt(gradient(u_k).^2 + epsilon), 2);
    pixel_count = length(gradient_values);
    Wk = spdiags([gradient_values gradient_values], [0 pixel_count], pixel_count, pixel_count);
    u_kp1 = (speye(pixel_count) - lambda * ( -Dx' * Wk * Dx - Dy' * Wk * Dy)) \ b;
