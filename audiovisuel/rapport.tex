% Basic stuff
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[a4paper, total={6.5in, 9.5in}]{geometry}
\usepackage[bookmarks, hidelinks, unicode]{hyperref}
\usepackage[french]{babel}
\usepackage[]{amsmath,amssymb}
\usepackage{stmaryrd}
\usepackage{tikz}
\usepackage{lmodern}
\usepackage{minted}
%\usepackage{soul}
\usepackage{float}

% Packages configuration
\usetikzlibrary{shapes.arrows, angles, quotes}
\renewcommand{\arraystretch}{1.4}
\restylefloat{table}

% Shortcut commands
\newcommand{\dx}{\mathrm d x}
\newcommand{\dy}{\mathrm d y}
\newcommand{\im}{\text{Im}\,}
\newcommand{\re}{\text{Re}\,}
\newcommand{\img}{\text{Img}\,}
\newcommand{\R}{{\mathbb R}}
\newcommand{\Y}{{\mathbb Y}}
\newcommand{\FEF}[1]{\underset{f:A\to B}{\text{FEF}}#1}
\newcommand{\C}{{\mathbb C}}
\newcommand{\N}{{\mathbb N}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\Q}{{\mathbb Q}}
\newcommand{\U}{{\mathbb U}}
\newcommand{\cC}{{\mathcal C}}
\newcommand{\cD}{{\mathcal D}}
\newcommand{\cF}{{\mathcal F}}
\newcommand{\cP}{{\mathcal P}}
\newcommand{\cL}{{\mathcal L}}
\newcommand{\cotan}{\operatorname{cotan}}
\newcommand{\conj}[1]{\overline{#1}}
\newcommand{\Aff}{\text{Aff}}
\newcommand{\twoRows}[1]{\multirow{2}{*}{#1}}
\newcommand{\threeRows}[1]{\multirow{3}{*}{#1}}
\newcommand{\twoCols}[1]{\multicolumn{2}{c|}{#1}}
\newcommand{\threeCols}[1]{\multicolumn{3}{|c|}{#1}}
\newcommand{\twoColsNB}[1]{\multicolumn{2}{c}{#1}}
\newcommand{\goesto}[2]{\xrightarrow[#1\:\to\:#2]{}}
\newcommand{\liminfty}{\lim_{x\to+\infty}}
\newcommand{\limminfty}{\lim_{x\to-\infty}}
\newcommand{\limzero}{\lim_{x\to0}}
\newcommand{\const}{\text{cste}}
\newcommand{\et}{\:\text{et}\:}
\newcommand{\ou}{\:\text{ou}\:}
\newcommand{\placeholder}{\diamond}
\newcommand{\mediateur}{\:\text{med}\:}
\newcommand{\milieu}{\:\text{mil}\:}
\newcommand{\vect}[1]{\overrightarrow{#1}}
\newcommand{\point}[2]{(#1;\;#2)}
\newcommand{\spacepoint}[3]{\begin{pmatrix}#1\\#2\\#3\end{pmatrix}}
\newcommand{\sh}{\operatorname{sh}}
\newcommand{\ch}{\operatorname{ch}}
\renewcommand{\th}{\operatorname{th}}
\newcommand{\id}{\operatorname{id}}
\renewcommand{\cong}{\equiv}
\newcommand{\converges}[2]{\xrightarrow[{#1\to #2}]{}}
\newcommand{\convergedby}[2]{\xleftarrow[{#1\to #2}]{}}
\newcommand{\md}{\textperiodcentered}

% Document

\author{Ewen Le Bihan}
\date{\today}
\title{Rapports de TPs, traitement audio-visuel}
\begin{document}

\maketitle
\tableofcontents

\newpage
\section{TP 5: Débruitage et inpainting}

\subsection{Débruitage}

On utilise un modèle de débruitage par variation totale. On cherche à minimiser les gradients entre chacun des pixels, en minimisant l'énergie

\begin{align*}
		\frac{1}{2} \iint_{\Omega} (u(x, y)-u_0(x, y))^2 + \lambda |\nabla u(x, y)|^2 \dx\dy
\end{align*}

\paragraph{}
L'implémentation Matlab utilise une méthode de résolution itérative sur une version discrétisée du problème d'optimisation de l'équation d'Euler-Lagrange liée à la minimisation de l'énergie précédente.

\paragraph{}
\begin{minted}{matlab}
function u_kp1 = debruitage(b,u_k,lambda,Dx,Dy,epsilon)
    gradient_values = mean(1 ./ sqrt(gradient(u_k).^2 + epsilon), 2);
    pixel_count = length(gradient_values);
    Wk = spdiags([gradient_values gradient_values], [0 pixel_count], pixel_count, pixel_count);
    u_kp1 = (speye(pixel_count) - lambda * ( -Dx' * Wk * Dx - Dy' * Wk * Dy)) \ b;
\end{minted}

\paragraph{}

Cette fonction traduit le calcul d'une itération basé sur l'équation

\begin{align*}
		(\underbrace{I_N - \lambda(-D_x^\top W^{(k)} D_x - D_y^\top W^{(k)} D_y}_{A^{(k)}}) u^{(k+1)} = \underbrace{u_0}_{b}
\end{align*}

\subsection{Inpainting par variation totale}

L'inpainting, qui consiste à supprimer une zone indésirable de l'image et à tenter de re-construire ce que se trouverait à la place de l'objet délimité par cette zone, peut être implémenté par variation totale, en reprenant l'algorithme précédent.

\paragraph{}
La différence est que l'on ne calcule plus le terme d'attache aux données de l'énergie sur la totalité de l'image ($\Omega$) mais sur l'image, privée de cette zone indésirable ($\Omega \setminus D$): en effet, on ne cherche pas à coller aux pixels originaux pour la zone indésirable. Ceci conduit à l'expression suivante de l'énergie à minimiser


\begin{align*}
		\frac{1}{2} \iint_{\mathbf{\Omega\setminus D}} (u(x, y)-u_0(x, y))^2 \dx\dy + \iint_{\Omega} \lambda |\nabla u(x, y)|^2 \dx\dy
\end{align*}

Cependant, le résultat n'est satisfaisant que si la zone indésirable n'a pas de composantes connexes trop grandes en aire:

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{tp-5/exercice_2_bis.png}
		\caption{Inpainting par variation totale, résultat satisfaisant}
		\label{fig:tp-5-exercice-2-bis}
\end{figure}

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{tp-5/exercice_2_ter.png}
		\caption{Inpainting par variation totale, résultat insatisfaisant}
		\label{fig:tp-5-exercice-2-ter}
\end{figure}

Les composantes connexes suffisamment grandes nous permette de voir que l'algorithme remplie les zones indésirables par une sorte de moyenne des pixels en bordure de ladite composante connexe, ce qui ne donne pas un résultat satisfaisant.

\subsection{Inpainting par rapiéçage}

L'inpainting par rapiéçage consiste à chercher des ``patches'' dans des zones non-indésirables de l'image qui ressemblent le plus à la zone indésirable, et à les utiliser pour remplacer celle-ci. Cet algorithme suppose que la zone indésirable est connexe. 

\paragraph{}
Pour chaque pixel $p$ de la bordure, on regarde dans une certaine fenêtre $F(p)$ centrée en $p$ tout les patches (zones carrées de pixels), et on garde le pixel central au patch le plus ressemblant au patch centré en  $p$, en calculant sa \emph{dissemblance}\footnote{La dissemblance est définie comme la moyenne des écarts carrés entre le pixel $p$ et les pixels  \emph{hors de la zone indésirable} du patch}.


\newpage
\section{TP 8: Décomposition structure/texture}

L'objectif ici est de séparer une image en deux complémentaires: une contenant le contenu fréquentiel haut de l'image (la \emph{texture}) et l'autre les fréquences basses (la \emph{structure}).

\paragraph{}
Cette formulation en termes fréquentiels amène naturellement à l'utilisation de transformées de Fourier pour décomposer l'image.

\subsection{Décomposition par modification du spectre}
Cette méthode consiste simplement à faire une transformée de Fourrier discrète sur l'image, ne garder que certains coefficients et appliquer une transofrmée inverse.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-8/exercice_1_1.png}
	\caption{Exemple}
	\label{fig:tp-8-exercice_1_1}
\end{figure}

\subsection{Approche variationnelle}

On minimise une énergie comportant deux termes:

\begin{description}
	\item[L'attache aux données] On souhaite que l'image \emph{structure} soit proche de l'image source
	\item[L'écart texture/structure] On souhaite que l'image structure comporte des gradients moins forts que l'image texture: l'intensité des gradients est une conséquence du fait de comporter des hautes fréquences
\end{description}

\paragraph{}
On résout la minimisation de l'énergie via un algorithme itératif sur une discrétisation de l'équation d'Euler-Lagrange associée.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-8/exercice_2.png}
	\caption{Exemple}
	\label{fig:tp-8-exercice_2}
\end{figure}

On a un facteur $\lambda$ qui permet de gérer l'importance que l'on accorde au critère d'écart texture/structure par rapport au critère d'attache aux données. L'exemple précédent utilise une valeur modérée de $\lambda = 100$. On peut observer que ce $\lambda$ traduit la proportion de fréquences hautes que l'on décide de ``ranger'' dans la texture. Le paramètre agit comme une sorte de fréquence de coupure, pour faire une analogie avec un filtre passe-bas en traitement audio.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{tp-8/exercice_2_lambda_1.png}
	\includegraphics[width=0.3\textwidth]{tp-8/exercice_2_lambda_10.png}
	\includegraphics[width=0.3\textwidth]{tp-8/exercice_2_lambda_1000.png}
	\caption{Exemple avec $\lambda \in \{1, 10, 1000\} $ (croissant de gauche à droite)}
	\label{fig:tp-8-exercice_other_lambdas}
\end{figure}

\newpage
\subsection{Une application à la détection de contours}

Cette séparation texture/structure nous donne au final deux images, plutôt qu'une, pour potentiellement guider un choix automatisé des seuils pour une détection de contours à hystérésis (seuil fort et seuil faible). C'est ce à quoi j'ai été confronté dans mon TIPE, qui portait sur la détection et classification de fractures osseuses à partir d'images radiographiques\footnote{Voir \url{https://github.com/ewen-lbh/bone-fracture-detection}}.

\paragraph{}

J'avais finit par faire du \emph{Deep Reinforcement Learning}  pour choisir des seuils en fonction d'informations comme la luminosité et le contraste, mais avoir deux images qui caractérise les hautes et basses fréquences d'une image peut être utile: la luminosité de l'image structure donne une bonne idée de la luminosité globale de l'image, et le contraste de l'image de texture donne une assez bonne idée de la \emph{finesse} des potentielles fractures: plus un trait de fracture est flagrant, plus la texture portera une marque contrastée.

\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\textwidth]{tp-8/exercice_2_bone_broken.png}
		\includegraphics[width=0.6\textwidth]{tp-8/exercice_2_bone_healthy.png}
		\caption{Application à de la détection de contours à finalité médicale}
		\label{fig:}
\end{figure}

\newpage
\section{TP 11: Reconnaissance musicale}

Le principe consiste à stocker des paires de \emph{pics spectraux} d'un morceau dans une base de données globale, en les associants au titre, artiste (et d'autres méta-données musicales telles que l'UNPC). Un\md e utilisateur\md ice de l'application peut ensuite enregistrer un extrait audio, en extraire des paires de pics spectraux et faire une recherche dans une base de données afin de trouver le morceau correspondant.

\subsection{Calcul des pics spectraux}
Les pics spectraux sont calculés sur le sonagramme: on prend le point de plus haute intensité parmi ses voisins, et ceux pour chaque point du sonagramme. Le sonagramme est calculé par transformée de Fourrier.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-11/exercice_1.png}
	\caption{Pics spectraux de \emph{Ewen-lbh — Ame to Yuki} }
	\label{fig:tp-11-exercice_1}
\end{figure}

\subsection{Création de paires}
Il faut ensuite créer des paires depuis ces pics spectraux. L'idée est que l'on apparie chaque point avec un autre s'ils sont suffisament proches en hauteur et temporellement, avec une condition supplémentaire pour la proximité temporelle: on n'apparie que ``dans un sens'', ce qui se traduit par le fait de ne considérer que les points ultérieurs au point en cours de traitement.

% TODO: figure

\subsection{Curiosité: Une anomalie sur l'analyse de morceaux ``ambient''}
En tant qu'artiste distribuant sur les plateformes, j'ai accès aux statistques Shazam de mes morceaux. J'ai remarqué quelque chose de particulier sur un de mes morceaux:


\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{./apple_music_shazam_anomaly.png}
	\caption{Statistiques Apple Music de \emph{Ewen-lbh — …until the novelty wears off}}
	\label{fig:shazam-anomaly}
\end{figure}

On peut remarquer qu'il y a \emph{très largement} plus de ``shazams''\footnote{Shazam est un service de reconnaissance musicale, qui a mis au point la technique de paires de pics spectraux} que d'écoutes, et c'est d'ailleurs peu réaliste qu'il y ait des shazams tout court étant donné la popularité de l'artiste et le caractère très ``ambiant'' du morceau: 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-11/exercice_1_shazam_anomaly.png}
	\caption{Sonogramme et pics de \emph{Ewen-lbh — …until the novelty wears off}}
	\label{fig:tp-11-exercice_1_shazam_anomaly}
\end{figure}

\paragraph{}
Ma théorie est que les pics sont tellement peu nombreux est à fréquence basse que Shazam à tendance à matcher mon morceau quand on tente de \emph{Shazamer} une ambiance calme sans musique apparente.

J'ai analysé\footnote{Les deux fichiers audio ont été convertis en OGG Vorbis avec échantillonage à 8 kHz via \emph{ffmpeg}, pour coller au reste des morceaux originellement présents dans \texttt{data.min}} un enregistrement d'un lieu très calme \cite{DeShon_2014}. Le résultat montre très peu de pics, dans les mêmes places de fréquence que le morceau. On peut imaginer qu'une ambiance un peu moins calme génèrera autant de pics que le morceau, à des emplacements fréquentio-temporels assez similaires pour induire Shazam en erreur.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-11/exercice_1_classroom.png}
	\caption{Analyse de \texttt{classroom\_ambiance.wav}}
	\label{fig:tp-11-exercice_1_classroom}
\end{figure}

\paragraph{}
On peut en conclure que beaucoup de gens essaie de shazamer du vide, par curiosité :)

\newpage
\section{TP 12: Séparation de sources musicales}

\subsection{Séparation contenu percussif/harmonique}
Cette méthode est basé sur le constant que les sons percussifs sont, sur un sonogramme, 
fréquentiellement larges et temporellement étroits, et inversement pour les sons harmoniques. 

\paragraph{}
En effet, un son harmonique comporte comparativement peut de fréquences différentes simultanées (sinon le cerveau ne peut ``se décider'' sur la note fondamentale à entendre), tandisque une percussion est considérablement plus proche du bruit blanc (c'est d'ailleurs en partant d'un bruit blanc que l'on peut créer la plupart des sons de caisse claire / hi hat /etc des musiques bas-débit ``chiptune''), et le bruit blanc comporte toutes les fréquences. Et temporellement, un son percussif est bref tandisqu'un son harmonique est (souvent) plus long.

\paragraph{}
En filtrant pour ces caractéristiques, on peut obtenir deux sonogrames complémentaires, l'un contenant les sons percussifs et l'autre le reste.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{tp-12/exercice_1_meganeko_source.png}
	\includegraphics[width=0.7\textwidth]{tp-12/exercice_1_meganeko_split.png}
	\caption{Exemple avec un morceau du genre ``Chiptune'' (\emph{meganeko — Milkshake}, de \texttt{00:01'30"} à \texttt{00:01'45"} )}
	\label{fig:exercice_1_meganeko}
\end{figure}

Sur cet exemple\footnote{Écoutable ici: \url{https://media.ewen.works/rapport-tav} }, la séparation se fait plutôt bien, excepté le \emph{kick}\footnote{grosse caisse}, qui apparaît plutôt dans la partie harmonique que percussive. On pourrait s'attendre à l'inverse, mais ce n'est en fait pas très surprenant: contrairement à la plupart des autres percussions, un kick n'occupe au final qu'une faible partie de la plage fréquentielle, et est comparable à une basse. 

\paragraph{}

C'est d'ailleurs tellement comparable à une basse que, en production musicale, on a souvent recours à une technique appelée \emph{sidechain} qui consiste à réduire le volume de la basse quand le kick est présent, parce que les deux instruments occupent la même plage fréquentielle mais sont tout les deux fondamentaux à un morceau, en particulier en musique électronique. Certains genres, comme la \emph{psytrance}, vont même plus loin en utilisant des basses rapides qui n'ont jamais de note en même temps que le kick.


\subsection{Curiosité: Interchanger percussivité et harmonicité}

L'observation précédente amène une question intéréssante: que donne un morceau de musique si l'on effectue une rotation de 90 degrés de son spectre, transformant ainsi les sons harmoniques en percussions et inversement?

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{tp-10/exercice_2_rot90.png}
		\caption{Rotation à 90 degrés du sonogramme de \emph{Audio/mpl.wav} (TP 10)}
		\label{fig:rot90}
\end{figure}

Résultat: \url{https://media.ewen.works/rapport-tav/rot90.mp3} (attention, ça peut faire mal aux oreilles, mais ça peut aussi faire office d'effet ``glitch'' pour un film de science-fiction avec des hackers)

\newpage
\section{TP 10: Manipulation de signaux audionumériques}

\subsection{Transformée de Fourier à court terme}
La \emph{TFTC} est une manière d'avoir une représentation qui présente \emph{à la fois} l'aspect temporel et fréquentiel d'un son.

\paragraph{}

De la même manière qu'on compteur de vitesse de voiture donne une vitesse ``instantanée'' expérimentale en ``trichant'' par l'emploi d'une fenêtre glissante d'un durée non-nulle pour calculer en fait une vitesse moyenne sur une durée courte, la TFTC calcule le spectre fréquentiel sur des durées courtes pour donner un \emph{sonogramme	}.

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{tp-10/exercice_1.png}
		\caption{Sonagramme de la publicité du \emph{Temps des tempêtes}}
		\label{fig:jumpscare}
\end{figure}

\subsection{Effets sur sonogramme: passe-bas}

Un moyen d'implémenter un passe-bas assez naturel est de simplement mettre à 0 tout les points d'un sonogramme dont la fréquence est supérieure à un seuil, appelé fréquence de coupure (ou \emph{cutoff})

\subsection{Passe-bas modulé par enveloppe}

En production musicale électronique, beaucoup de sons de synthétiseurs sont créés via des instruments virtuels (\emph{VST}\footnote{Virtual Studio Technology}) permettant de la synthèse dite \emph{additive}: on part d'oscillateurs, qui produisent des ondes de formes élémentaires (sinusoïde, \emph{saw}\footnote{Abbréviation courante de \emph{sawtooth}, signifiant ``dent de scie''}, carré, triangle,…) dont la fréquence est controllé par la hauteur de la note jouée au clavier. Puis, on donne à ces oscillateurs une enveloppe ADSR (attack, decay, sustain, release) modulant leur amplitude. Enfin, il est assez commun d'appliquer un filtre passe-bas au signal, le signal brut pouvant être assez perçant (en particulier avec des saw). Enfin, manipuler la fréquence de coupure permet de donner du mouvement au son du synthétiseur: on dit que l'on \emph{ouvre le filtre} quand on augment sa fréquence de coupure.

\paragraph{}

Beaucoup de sons de synthés rythmés en musique électronique lie l'ouverture du filtre à l'enveloppe: quand on joue une note, le filtre s'ouvre puis se referme vers la fin de la note.

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{serum.png}
		\caption{Un synthé construit avec \emph{Serum}, qui module le cutoff du filtre (section \emph{FILTER}) avec l'enveloppe (section \emph{ENV1}), dont la plage de modulation est indiqué par la section bleue sur le potentiomètre \emph{CUTOFF}}
		\label{fig:serum}
\end{figure}

\paragraph{}

L'idée est de tenter de reproduire cet effet \emph{sans accès au données MIDI (donc au timings des notes telles qu'elles sont jouées}, en se basant simplement sur le signal audio d'un synthé dont le filtre n'est pas modulé par son enveloppe

\paragraph{}

Pour se faire, on va détecter les pics d'amplitude dans le signal puis appliquer un filtre passe bas dont la fréquence de coupure dépend du temps, ce qui se traduit parl'application d'un masque \emph{diagonal} sur le sonogramme.

\subsubsection{Passe-bas progressif}
On commence par tester un filtre passe-bas dit ``progressif'', qui possède deux fréquences de coupure: celle du début du traitement, et celle de la fin du traitement. On fourni aussi la durée sur laquelle cette progression du cutoff doit s'effectuer. On interpolera linéairement par souci de simplicité, mais les VST offrent d'utiliser d'autres courbes, dont celle de l'amplitude.

\begin{minted}{matlab}
function out = passe_bas_progressif(in, frequencies, starting_cutoff, ending_cutoff, duration)
    out = in;
    cutoff_step_size = (ending_cutoff - starting_cutoff) / duration;
    for i = 1:size(in, 2)
        cutoff = starting_cutoff + cutoff_step_size * i;
        out(frequencies > cutoff, i) = 0;
    end
\end{minted}

\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{tp-10/exercice_2_lowpass_cutoff_progressive_500_to_10k.png}
		\caption{Passe-bas ``progressif'' de 500 Hz à 10 kHz}
		\label{fig:cutoff_progressive}
\end{figure}

Résultat: \url{https://media.ewen.works/rapport-tav/passe_bas_progressif.mp3}

\paragraph{}
On note aussi que ouvrir progressivement un passe bas depuis 0 Hz jusqu'à une fréquence assez élevée pour que le filtre n'agisse plus (10 kHz pour être sûr\md e de soi) est également un moyen courant d'introduire un instrument (par exemple, la mélodie principale au début de \url{https://media.ewen.works/rapport-tav/ewen-lbh-petrichor.mp3}\footnote{exclu de mon prochain album ``émoti*ns'' d'ailleurs})

\subsection{Contour d'enveloppe}
Enfin, pour obtenir l'effet désiré, on applique cette rampe à chaque fois que l'on détecte que la moyenne des intensités à un instant est supérieur à la moyenne maximale, plus ou moins un certain $\varepsilon$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{tp-10/exercice_2_adsr_cutoff_rebuilt.png}
	\caption{Exemple sur un son de synthé}
	\label{fig:tp-10-exercice_2_adsr_cutoff}
\end{figure}

\begin{description}
	\item[Son original] 
\url{https://media.ewen.works/rapport-tav/synth_no_cutoff.mp3}
\item[Effet reconstitué]  
\url{https://media.ewen.works/rapport-tav/synth_cutoff_rebuilt.mp3}
\item[Effet original] 
\url{https://media.ewen.works/rapport-tav/synth_with_cutoff.mp3}
\item[Synthé dans son contexte] 
\url{https://media.ewen.works/rapport-tav/synth_full_context.mp3}
\end{description}



\bibliographystyle{plain} % We choose the "plain" reference style
\bibliography{rapport} % Entries are in the refs.bib file

\end{document}
