type 'a tree = 
| Node of 'a * 'a tree * 'a tree
| Empty

let rec appartient (path: 'a list)  = function
| Empty -> false
| Node (x, Empty, Empty) -> path = [x]
| Node (x, t1, t2) -> match path with
  | [] -> false
  | h::t -> if h = x then appartient t t1 || appartient t t2 else false
