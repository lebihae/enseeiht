(*  Exercice à rendre **)

(*  Contrat : pgcd int -> int -> int *)
(* Pré-conditions: a >= 0, b >= 0, (a, b) != (0, 0) *)

let rec pgcd a b = match (a, b) with
| _ when a < 0 || b < 0 -> failwith "a, b must be in ℕ"
| _ when a = b -> a
| (0, 0) -> failwith "a, b must not be both 0"
| (0, b) -> b
| (a, 0) -> a
| _ when a > b -> pgcd (a-b) b
| _ when a < b -> pgcd a (b-a)
| _ -> failwith "unreachable"

(* Tests *)

let%test _ = pgcd 42 56  = 14
let%test _ = pgcd 461952 116298  = 18
let%test _ = pgcd 7966496 314080416  = 32
let%test _ = pgcd 24826148 45296490  = 526
let%test _ = pgcd 12 0  = 12
let%test _ = try pgcd 0 0  = -1 with Failure _ -> true
let%test _ = pgcd 0 9  = 9
let%test _ = try pgcd (-2) (-2) = -1 with Failure _ -> true
