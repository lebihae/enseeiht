(* Exercice 2 *)

(* ajout: fonction qui à partir d'un élément e et d'une liste 2 fois plus grande qui contient la liste originale + la liste où on a ajouté e à chaque ensemble paramètres

  ajout: 'a -> 'a list list -> 'a list list
*)

(* let rec ajout e ll = match ll with 
| [] -> [[e]; []]
| l::qq -> l::(e::l)::(ajout e qq) *)

let ajout e ll = List.flatten (List.map (fun l -> [e::l; l]) ll)

(* Exercice 3 *)

let rec insertions e l = match l with
| [] -> [[e]]
| h::t -> (e::l) :: (List.map (fun r -> h::r) (insertions e t))

let rec permutations l = match l with
| [] -> [[]]
| h::t -> List.flatten (List.map (fun r -> insertions h r) (permutations t))

(* Exercice 4 *)

