clear;
close all;

load eclairages;
load donnees_objet_3D;

[rho,N] = rti3D(images_objet_3D,theta,phi);

% Integration du champ de normales :
exterieur = find(masque_objet_3D==0);		% Exterieur du domaine de reconstruction
N(exterieur,3) = 1;				% Pour eviter les divisions par 0
p = reshape(-N(:,1)./N(:,3),size(masque_objet_3D));
p(exterieur) = 0;
q = reshape(-N(:,2)./N(:,3),size(masque_objet_3D));
q(exterieur) = 0;
z = integration_SCS(q,p);
z(exterieur) = NaN;

% Affichage du resultat :
figure('Name','Reconstruction 3D','Position',[0.5*L,0.25*H,0.5*L,0.75*H]);
h = surf(z,repmat(rho,1,1,3));
shading flat;
colormap gray;
axis equal;
axis off;
view(0,90);					% Direction d'eclairage
hc = camlight('headlight','infinite');
view(-44,42);					% Direction d'observation
