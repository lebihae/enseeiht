clear;
close all;
taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);

load donnees_spheres;

% Determination des directions (theta,phi) des m eclairages :
[theta,phi] = etalonnage(images_spheres, rayon_sphere);
% Affichage des directions d'eclairage dans le plan (theta,phi) :
figure('Name','Directions d''eclairage','Position',[0.65*L,0.45*H,0.35*L,0.55*H]);
plot(theta,phi,'x','Color','r','LineWidth',4,'MarkerSize',10);
axis([0,pi/2,-pi,pi]);
set(gca,'FontSize',20);
xlabel('$\theta$','Interpreter','Latex','FontSize',30);
ylabel('$\phi$','Interpreter','Latex','FontSize',30);

save eclairages L H theta phi;
