clear;
close all;

load eclairages;
load donnees_objet_3D;

[nb_lignes,nb_colonnes,m] = size(images_objet_3D);

% Calcul du spline d'interpolation du niveau de gris en chaque pixel :
splines = interpolation(images_objet_3D,theta,phi);

% Simulation d'un eclairage tournant :
figure('Name','Simulation d''un eclairage tournant','Position',[0.5*L,0.25*H,0.5*L,0.75*H]);
theta = 1.2;
sin_theta = sin(theta);
valeurs_phi = 0:0.1:2*pi+0.05;
x_centre = nb_colonnes/2;
y_centre = nb_lignes/2;
for k = 1:length(valeurs_phi)
	phi = valeurs_phi(k);
	l = [1 theta phi theta^2 theta*phi phi^2];
	image = max(255*l*splines,0);
	image = reshape(image,[nb_lignes,nb_colonnes]);
	imagesc(image);
	axis xy;
	colormap gray;
	axis off;
	pause(0.1);
end
