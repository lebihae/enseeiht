function [theta, phi] = etalonnage(images_spheres, rayon_sphere)
taille_noyau = round(rayon_sphere/20);
triangle = @(n)(1+n-abs(-n:n))/n;
[~, ~, images_count, spheres_count] = size(images_spheres);

for  img_idx = 1:images_count
    phis = zeros(spheres_count, 1);
    thetas = zeros(spheres_count, 1);

    for sphere_idx = 1:spheres_count
        image_ik = images_spheres(:, :, img_idx, sphere_idx);
        image_ik = conv2(triangle(taille_noyau),triangle(taille_noyau),image_ik,'same');
        [~, maxidx] = max(image_ik(:));
        [x_orig, y_orig] = ind2sub(size(image_ik), maxidx);
        maxcoords_from_center = [x_orig, y_orig] - rayon_sphere;
        y = maxcoords_from_center(1);
        x = maxcoords_from_center(2);
        phis(sphere_idx) = atan2(y, x);
        r = sqrt(x^2 + y^2);
        thetas(sphere_idx) = 2 * asin(r / rayon_sphere);
    end

    phi(img_idx) = mean(phis);
    theta(img_idx) = mean(thetas);
end


end
