function splines = interpolation(images_objet_3D,theta,phi)
    [width, height, m] = size(images_objet_3D);
    n = width*height;
    I = reshape(images_objet_3D, n, m)';
    E = [ones(m, 1) theta' phi' theta'.^2 theta'.*phi' phi'.^2];
    splines = double(E)\double(I);
end