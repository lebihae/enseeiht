function [rho,N] = rti3D(images_objet_3D,theta,phi)
    [width, height, m] = size(images_objet_3D);
    n = width*height;
    I = reshape(images_objet_3D, n, m)';
    [rhos, i] = max(double(I));
    rho = reshape(rhos, [width,height]);
    N = [
        sin(theta(1, i)).*cos(phi(1, i))
        sin(theta(1, i)).*sin(phi(1, i))
        cos(theta(1, i))
    ];
end