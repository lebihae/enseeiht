#!/usr/bin/env fish
set black_color "#202124"
set white_color "#ffffff"

set here (dirname (status --current-filename))

for figure in $here/*fig*.svg
    sd "<rect x=\"[\d.]+\" y=\"[\d.]+\" width=\"[\d.]+\" height=\"[\d.]+\" fill=\"$black_color\" />" "" "$figure"
    sd "<rect x=\"[\d.]+\" y=\"[\d.]+\" width=\"[\d.]+\" height=\"[\d.]+\" fill=\"#000000\" />" "" "$figure"
	sd $white_color "#000000" "$figure"
    sd $black_color "#ffffff" "$figure"
	# sd '\bstroke-width:2\b' 'stroke-width:20' "$figure"
	magick $figure (dirname $figure)/(basename $figure .svg).png
	# rm $figure
	echo "Converted $figure"
end

# pdflatex -output-directory= -interaction=batchmode $here/*/*.tex

