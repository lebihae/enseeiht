function [tirage_estim, E_estim] = estimation_E_robuste(q1, q2, K)
    epipolar_line_distance_median = Inf;

    size(q1)

    while epipolar_line_distance_median > 2
        points_to_test = randperm(size(q1, 2), 8);
        q1_subset = q1(:, points_to_test);
        q2_subset = q2(:, points_to_test);
        q1_subset(:, 3)
        E_estim = estimation_E(q1_subset, q2_subset);
        % D₂ : q₁ᵀEq₂ = 0
        % D₁ : q₂ᵀEq₁ = 0
        % iterate points in chunks of 2
        distances = [];
        for i = 1:min(size(q1, 2), size(q2, 2))
            x1 = q1(1, i);
            y1 = q1(2, i);
            z1 = q1(3, i);
            x2 = q2(1, i);
            y2 = q2(2, i);
            z2 = q2(3, i);

            % p1_tilde = K * q1;
            % p2_tilde = K * q2;

            distance_p1 = ([x1 y1 z1] ) .^2  / (x1^2 + y1^2);
            distance_p2 = ([x2 y2 z2] * p1_tilde) .^2  / (x2^2 + y2^2);

            distances = [distances distance_p1 distance_p2];
        end

        epipolar_line_distance_median = median(distances)
    end
end
