function E_estim = estimation_E(q1_estim, q2_estim)
    points_count = size(q1_estim, 2)
    A = zeros(points_count, 9);
    for i = 1:points_count
        A(i, :) = kron(q1_estim(:, i)', q2_estim(:, i)');
    end


    [~, ~, E_estim] = svd(A);

    E_estim = reshape(E_estim(:, end), 3, 3)';

%     [E_estim, ~] = eig(A'*A);

% size(E_estim)

    [U, ~, V] = svd(E_estim);
     E_estim = U * [1 0 0; 0 1 0; 0 0 0] * V';
end
