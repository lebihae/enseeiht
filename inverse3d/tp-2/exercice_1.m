clear;
close all;

load E_estimee;

% Estimation de la pose (4 solutions) :
[t_4,R_4] = estimation_4_poses(E_estimee);

% Verification :
verification = zeros(1,4);
for i = 1:4
	t = t_4(:,i);
	t_matrice = [0 -t(3) t(2) ; t(3) 0 -t(1) ; -t(2) t(1) 0];
	R = R_4(:,:,i);
	matrice_verif = (t_matrice*R)./E_estimee;
	verification(i) = abs(abs(prod(matrice_verif(:)))-1) < 1e-2;
end

if prod(verification)
	fprintf('Vous avez tout bon : passez a l''exercice 2 !\n');
else
	fprintf('Erreur : revoyez votre code, svp !\n');
end
