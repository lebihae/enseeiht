function affichage_3D(I1,p1_apparies,Q,t,R,num_figure)

% Recuperation de la couleur des points 3D :
[nb_lignes,nb_colonnes,nb_canaux] = size(I1);
nb_pixels = nb_lignes*nb_colonnes;
couleurs = reshape(I1,[nb_pixels,nb_canaux]);
j_p1_apparies = round(p1_apparies(1,:));	% u correspond à j
i_p1_apparies = round(p1_apparies(2,:));	% v correspond à i
indices_couleurs = sub2ind([nb_lignes,nb_colonnes],i_p1_apparies,j_p1_apparies);
couleurs_Q = couleurs(indices_couleurs,:);

% Affichage des cameras :
taille_camera = 0.2;
if nargin == 6
	subplot(2,2,num_figure);
end
plotCamera('Size',taille_camera,'Color','r','Label','1','Opacity',0);
hold on;
grid on;
position = single(-R'*t);
orientation = single(R');
absPose = rigidtform3d(orientation,position);
plotCamera('AbsolutePose',absPose,'Size',taille_camera,'Color','b','Label','2','Opacity',0);
axis off;

% Affichage du nuage de points 3D :
nuage_Q = pointCloud(transpose(Q),'Color',couleurs_Q);
pcshow(nuage_Q,'VerticalAxis','y','VerticalAxisDir','down','MarkerSize',45);
