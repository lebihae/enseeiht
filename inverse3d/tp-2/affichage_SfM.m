function affichage_SfM(Q,couleurs_Q,t_cameras,R_cameras,couleurs_cameras)

% Affichage des cameras :
taille_camera = 0.5;
for i = 1:size(t_cameras,2)
	t_i = t_cameras(:,i);
	R_i = R_cameras(:,:,i);
	position = single(t_i'*R_i);		% Matlab a sa propre logique !
	orientation = single(R_i');
	absPose = rigid3d(orientation,position);
	if nargin==5
		plotCamera('AbsolutePose',absPose,'Size',taille_camera,...
			'Color',couleurs_cameras(:,i),'Label',num2str(i),'Opacity',0);
	else
		plotCamera('AbsolutePose',absPose,'Size',taille_camera,...
			'Color','b','Label',num2str(i),'Opacity',0);
	end		
	hold on;
end
grid on;
axis off;

% Affichage du nuage de points 3D :
nuage_Q = pointCloud(Q','Color',couleurs_Q');
pcshow(nuage_Q,'VerticalAxis','y','VerticalAxisDir','down','MarkerSize',45);
