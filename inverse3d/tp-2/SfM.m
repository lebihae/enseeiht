clear;
close all;
taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);

load Fontaine/matrice_K;
inverse_K = inv(K);

% Lecture des images :
n = 4;
for i = 1:n
	nom_fichier = strcat('Fontaine/im',int2str(i),'.png');
	images{i} = imread(nom_fichier);
end

% Couleurs des n cameras (et des n-1 nuages de points 3D) :
couleurs_cameras = uint8([ 255 0 0 ; 0 255 0 ; 0 0 255 ; 255 255 255 ]);
couleurs_cameras = transpose(couleurs_cameras);

% Structure-from-Motion :
E = [];
t = [];
R = [];
Q = [];
couleurs_Q = [];
for i = 1:n-1

	% Paire d'images courante :
	I1_i = images{i};
	I2_i = images{i+1};

	% Detection des points d'interet dans l'image gauche :
	RI1_i = selection_RI(I1_i,L,H);
	p1_i = detectMinEigenFeatures(rgb2gray(I1_i),'ROI',RI1_i,'MinQuality',0.0001);

	% Mise en correspondance des points d'interet :
	traqueur = vision.PointTracker('MaxBidirectionalError',1,'NumPyramidLevels',5);
	p1_i = p1_i.Location;
	initialize(traqueur,p1_i,I1_i);
	[p2_i,indices_droite] = step(traqueur,I2_i);
	p1_apparies_i = p1_i(indices_droite,:);
	p2_apparies_i = p2_i(indices_droite,:);

	% Transposition de p1_apparies_i et p2_apparies_i, pour "coller" aux notations du cours :
	p1_apparies_i = transpose(p1_apparies_i);
	p2_apparies_i = transpose(p2_apparies_i);

	% Coordonnees homogenes des pixels apparies :
	nb_paires = size(p1_apparies_i,2);
	p1_tilde_i = [p1_apparies_i ; ones(1,nb_paires)];
	p2_tilde_i = [p2_apparies_i ; ones(1,nb_paires)];

	% Points 3D w :
	w1_i = inverse_K*p1_tilde_i;
	w2_i = inverse_K*p2_tilde_i;

	% S'il y a trop de paires, on n'en conserve que 10000 :
	nb_paires_majore = min(nb_paires,10000);
	indices = randperm(nb_paires);
	indices_selection = indices(1:nb_paires_majore);
	w1_i_selection = w1_i(:,indices_selection);
	w2_i_selection = w2_i(:,indices_selection);

	% Estimation robuste de E :
	[~,E_i] = estimation_E_robuste(w1_i_selection,w2_i_selection,K);

	% Variables qui seront utilisees dans l'exercice 3 :
	E = cat(3,E,E_i);
	p1_tilde{i} = p1_tilde_i;
	p2_tilde{i} = p2_tilde_i;

	% Estimation du changement de pose (t,R) :
	[t_4,R_4] = estimation_4_poses(E_i);
	[t_i,R_i] = estimation_pose(t_4,R_4,w1_i,w2_i);
	t = [t,t_i];
	R = cat(3,R,R_i);

	% Reconstruction 3D (methode "du point milieu", cf. TP1) :
	Q_i = reconstruction_3D(w1_i,w2_i,t_i,R_i);

	% Points 3D reconstruits, exprimes dans le repere de la camera i+1 :
	if i==1
		Q = Q_i;
	else
		Q = [ R_i*Q+repmat(t_i,1,size(Q,2)) Q_i ];
	end

	% Couleur affectee au i-eme nuage de points 3D :
	couleurs_Q = [ couleurs_Q repmat(couleurs_cameras(:,i),1,size(Q_i,2)) ];
end

% Affichage du resultat :
close all;
figure('Name','Reconstruction 3D par SfM','Position',[0.1*L,0,0.8*L,H]);
t_cameras = zeros(3,1);
R_cameras = eye(3);
for i = 2:n
	R_cameras(:,:,i) = R(:,:,n-i+1)'*R_cameras(:,:,i-1);
	t_cameras(:,i) = -R_cameras(:,:,i)*t(:,n-i+1)+t_cameras(:,i-1);
end
affichage_SfM(Q,couleurs_Q,t_cameras,R_cameras,couleurs_cameras);

save donnees_SfM;
