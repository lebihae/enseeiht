clear;
close all;
taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);

load donnees_SfM;

% Couleurs des n cameras :
couleurs_cameras = uint8([ 255 0 0 ; 0 255 0 ; 0 0 255 ; 255 255 255 ]);
couleurs_cameras = transpose(couleurs_cameras);

% Structure-from-Motion :
Q = [];
couleurs_Q = [];
for i = 1:n-1

	% Matrice essentielle et changement de pose :
	E_i = E(:,:,i);
	t_i = t(:,i);
	R_i = R(:,:,i);

	% Coordonnees homogenes des pixels apparies :
	p1_tilde_i = p1_tilde{i};
	p2_tilde_i = p2_tilde{i};

	% Points 3D w :
	w1_i = inverse_K*p1_tilde_i;
	w2_i = inverse_K*p2_tilde_i;

	% Estimation de l'echelle :
	if i>1
		% Paires de points d'interet associees a la paire d'images {I_{i-1},I_i}} :
		p2_tilde_i_moins_1 = p2_tilde{i-1};
		[~,ind_i_moins_1,ind_i] = intersect(round(p2_tilde_i_moins_1'),round(p1_tilde_i'),'rows');

		% Estimation du facteur d'echelle :
		alpha_i = estimation_echelle(Q_i_moins_1(:,ind_i_moins_1),w2_i(:,ind_i),t_i,R_i);
		t_i = alpha_i*t_i;
	end

	% Mise a jour de t :
	t(:,i) = t_i;

	% Reconstruction 3D (methode "du point milieu", cf. TP1) :
	Q_i = reconstruction_3D(w1_i,w2_i,t_i,R_i);

	% Points 3D reconstruits, exprimes dans le repere de la camera i+1 :
	if i==1
		Q = Q_i;
	else
		Q = [ R_i*Q+repmat(t_i,1,size(Q,2)) Q_i ];
	end
	Q_i_moins_1 = Q_i;

	% La couleur d'un point 3D est sa couleur dans l'image gauche :
	I1_i = images{i};
	for j = 1:size(p1_tilde_i,2)
		currentColor = I1_i(round(p1_tilde_i(2,j)),round(p1_tilde_i(1,j)),:);
		couleurs_Q = [ couleurs_Q reshape(currentColor,3,1) ];
	end
end

% Affichage du resultat :
close all;
figure('Name','Reconstruction 3D par SfM','Position',[0.1*L,0,0.8*L,H]);
t_cameras = zeros(3,1);
R_cameras = eye(3);
for i = 2:n
	R_cameras(:,:,i) = R(:,:,n-i+1)'*R_cameras(:,:,i-1);
	t_cameras(:,i) = -R_cameras(:,:,i)*t(:,n-i+1)+t_cameras(:,i-1);
end
affichage_SfM(Q,couleurs_Q,t_cameras,R_cameras,couleurs_cameras);
