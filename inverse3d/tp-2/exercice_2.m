clear;
close all;
taille_ecran = get(0,'ScreenSize');
L = taille_ecran(3);
H = taille_ecran(4);

load donnees_appariees;
load E_estimee;

% Coordonnees homogenes des pixels apparies :
p1_tilde = [p1_apparies ; ones(1,nb_paires)];
p2_tilde = [p2_apparies ; ones(1,nb_paires)];

% Points 3D w :
w1 = inverse_K*p1_tilde;
w2 = inverse_K*p2_tilde;

% Estimation de la pose (4 solutions) :
[t_4,R_4] = estimation_4_poses(E_estimee);

% Reconstruction 3D (4 solutions) :
figure('Name','Reconstruction 3D : 4 solutions','Position',[0.1*L,0,0.8*L,H]);
for i = 1:4
	t = t_4(:,i);
	R = R_4(:,:,i);
	Q = reconstruction_3D(w1,w2,t,R);
	affichage_3D(I1,p1_apparies,Q,t,R,i);
end

input('Tapez Entree pour afficher la bonne solution !');

% Determination de la "bonne" pose :
[t_estimee,R_estimee] = estimation_pose(t_4,R_4,w1,w2);

% Reconstruction 3D de la "bonne" solution :
Q = reconstruction_3D(w1,w2,t_estimee,R_estimee);

% Affichage de la "bonne" solution :
close;
figure('Name','Reconstruction 3D','Position',[0.1*L,0,0.8*L,H]);
affichage_3D(I1,p1_apparies,Q,t_estimee,R_estimee);
