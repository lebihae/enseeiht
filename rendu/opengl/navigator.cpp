#include <cstdlib>

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <math.h>
#include <iostream>
using namespace std;

#define SPEED 0.1         // OpenGL unit
#define ANGULAR_SPEED 0.5 // degrees
#define TAU 2. * M_PI

float camX = 0.0;
float camY = 0.0;
float camZ = 5.0;
float camAngleX = 0.0;
float camAngleY = 0.0;

float deg2rad(float deg)
{
    return deg / 180. * TAU;
}

float noramlizeDegreesAngle(float degrees)
{
    return fmod(degrees, 360);
}


float finalCamX()
{
    return camX + camAngleX * (cos(deg2rad(camAngleX)));
}

float finalCamY()
{
    return camY + camAngleY * (1 - sin(deg2rad(camAngleY)));
}

void display()
{
    // clear window
    glClear(GL_COLOR_BUFFER_BIT);

    // set current matrix as GL_MODELVIEW
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(finalCamX(), finalCamY(), camZ, camX, camY, -1., .0, 1., .0);
    printf("eye = (%f, %f, %f)\n", finalCamX(), finalCamY(), camZ);

    // add a copy of the curr. matrix to the stack
    glPushMatrix();

    glPushMatrix();
    // translate by -3 on the z
    glTranslatef(.0f, .0f, -3.f);
    // set drawing color to red
    glColor3f(1.0f, 0.0f, 0.0f);
    // middle red teapot
    glutWireTeapot(1);
    // translate by 2 on the y
    glTranslatef(.0f, 2.f, .0f);
    // set drawing color to blue
    glColor3f(.0f, 1.0f, .0f);
    // rotate 90 deg around x
    glRotatef(90.f, 1.0f, 0.0f, 0.0f);
    // top green teapot
    glutWireTeapot(1);
    // pop the current matrix
    glPopMatrix();

    // translate -2 on y and -1 on z
    glTranslatef(.0f, -2.f, -1.f);
    // set drawing color to blue
    glColor3f(.0f, .0f, 1.f);
    // bottom blue teapot
    glutWireTeapot(1);

    // pop the current matrix
    glPopMatrix();

    // flush drawing routines to the window
    glFlush();
}

void arrowKey(int key, int, int)
{
    switch (key)
    {
    case GLUT_KEY_LEFT:
        camAngleX -= ANGULAR_SPEED;
        break;
    case GLUT_KEY_RIGHT:
        camAngleX += ANGULAR_SPEED;
        break;
    case GLUT_KEY_UP:
        camAngleY += ANGULAR_SPEED;
        break;
    case GLUT_KEY_DOWN:
        camAngleY -= ANGULAR_SPEED;
        break;
    default:
        break;
    }
    glutPostRedisplay();
}

// Function called everytime a key is pressed
void key(unsigned char key, int, int)
{
    switch (key)
    {
    case 27:
        exit(EXIT_SUCCESS);
        break;
    case 'z':
        camZ -= SPEED;
        break;
    case 's':
        camZ += SPEED;
        break;
    case 'q':
        camX -= SPEED;
        break;
    case 'd':
        camX += SPEED;
        break;
    case 'a':
        camY -= SPEED;
        break;
    case 'e':
        camY += SPEED;
        break;
    }
    glutPostRedisplay();
}

void reshape(int width, int height)
{
    // define the viewport transformation
    glViewport(0, 0, width, height);
}

int main(int argc, char *argv[])
{
    // initialize GLUT, using any commandline parameters passed to the program
    glutInit(&argc, argv);

    // setup the size, position, and display mode for new windows
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_RGB);

    // create and set up a window
    glutCreateWindow("hello, teapot!");
    // Set up the callback functions
    // for display
    glutDisplayFunc(display);
    // for the keyboard
    glutKeyboardFunc(key);
    glutSpecialFunc(arrowKey);
    // for reshaping
    glutReshapeFunc(reshape);

    // define the projection transformation
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, 1, 1, 10);

    // define the viewing transformation
    glMatrixMode(GL_MODELVIEW);

    // tell GLUT to wait for events
    glutMainLoop();
}
