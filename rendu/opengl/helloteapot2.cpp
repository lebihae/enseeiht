#include <GL/gl.h>
#include <GL/freeglut.h>
#include <iostream>

bool wireframing = false;
int fov = 100;
int zNear = 1;
float eyeX = 1;
float eyeY = 1.2;
float eyeZ = 0;

float lookAtX = 1;
float lookAtY = 1.2;
float lookAtZ = 0;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, 1, zNear, 10);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    std::cout << "eye at (" << eyeX << ", " << eyeY << ", " << eyeZ << ")" << std::endl;
    std::cout << "looking at (" << lookAtX << ", " << lookAtY << ", " << lookAtZ << ")" << std::endl;
    gluLookAt(eyeX, eyeY, eyeZ, lookAtX, lookAtY, lookAtZ, 0., 1., 0.);

    if (wireframing)
    {
        glutWireTeapot(.5);
    }
    else
    {
        glutSolidTeapot(0.5);
    }

    glFlush();
}

void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
}

void keyboard(unsigned char key, int x, int y)
{
    float upStep = 0.25;
    float lookAtStep = 0.25;

    switch (key)
    {

    case 27:
        eyeX = 1;
        eyeY = 2.2;
        eyeZ = 1;
        fov = 60;
        lookAtX = 0;
        lookAtY = 0;
        lookAtZ = 0;
        break;

    case 'q':
        eyeY += upStep;
        break;
    case 'd':
        eyeY -= upStep;
        break;

    case 'w':
        wireframing = !wireframing;
        break;

    case 't':
        eyeZ += upStep;
        break;

    case 'y':
        eyeZ -= upStep;
        break;

    case 'z':
        eyeX += upStep;
        break;
    case 's':
        eyeX -= upStep;
        break;

    case 'p':
        fov -= 10;
        break;
    case 'm':
        fov += 10;
        break;

    case 'i':
        lookAtY += lookAtStep;
        break;
    case 'k':
        lookAtY -= lookAtStep;
        break;
    case 'j':
        lookAtY -= lookAtStep;
        break;
    case 'l':
        lookAtY += lookAtStep;
        break;
    case 'u':
        lookAtZ += lookAtStep;
        break;
    case 'o':
        lookAtZ -= lookAtStep;
        break;

    default:
        break;
    }

    glutPostRedisplay();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);

    // glutInitWindowSize(1920 / 2, 1080 / 2);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_RGB);

    glutCreateWindow("Hiya~");

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);

    glutMainLoop();
}
