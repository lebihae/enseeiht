
import algebra.*;

/**
 * author: cdehais
 */
public class Transformation {

    Matrix worldToCamera;
    Matrix projection;
    Matrix calibration;

    public Transformation() {
        try {
            worldToCamera = new Matrix("W2C", 4, 4);
            projection = new Matrix("P", 3, 4);
            calibration = Matrix.createIdentity(3);
            calibration.setName("K");
        } catch (InstantiationException e) {
            /* should not reach */
        }
    }

    public void setLookAt(Vector3 cam, Vector3 lookAt, Vector3 up) {
        try {
            // compute rotation
            // Vector3 normalizedCam = new Vector3(cam);
            // normalizedCam.normalize();
            // Vector3 normalizedLookAt = new Vector3(lookAt);
            // normalizedLookAt.normalize();

            // Vector3 v = normalizedCam.cross(normalizedLookAt);
            // double s = v.norm();
            // double c = normalizedCam.dot(normalizedLookAt);

            // Matrix skewSymmetric = new Matrix(3, 3);
            // skewSymmetric.set(0, 1, -v.get(2));
            // skewSymmetric.set(0, 2, v.get(1));
            // skewSymmetric.set(1, 0, v.get(2));
            // skewSymmetric.set(1, 2, -v.get(0));
            // skewSymmetric.set(2, 0, -v.get(1));
            // skewSymmetric.set(2, 1, v.get(0));

            // Matrix rotation = Matrix.createIdentity(3).add(skewSymmetric)
            // .add(skewSymmetric.square().multiply((1 - c) / (s * s)));

            // // compute translation
            // Matrix transformation = new Matrix(4, 4);

            // for (int i = 0; i < 3; i++) {
            // transformation.set(i, 3, -cam.get(i));
            // }
            // for (int i = 0; i < 3; i++) {
            // for (int j = 0; j < 3; j++) {
            // transformation.set(i, j, rotation.get(i, j));
            // }
            // }
            // transformation.set(4, 4, 1);

            Vector3 e3 = lookAt.difference(cam);
            e3.normalize();
            Vector3 e1 = up.cross(e3);
            e1.normalize();
            Vector3 e2 = e3.cross(e1);

            Matrix N = new Matrix(3, 3);
            N.setColumn(0, e1);
            N.setColumn(0, e2);
            N.setColumn(0, e3);

            Vector3 t = (Vector3) N.multiply(-1).multiply(cam);

            worldToCamera = new Matrix(4, 4);
            worldToCamera.setSubmatrix(0, 0, N.transpose());
            worldToCamera.setColumn(3, t);
            worldToCamera.set(3, 3, 1);
        } catch (Exception e) {
            /* unreached */ }
        ;

        System.out.println("Modelview matrix:\n" + worldToCamera);
    }

    public void setProjection() {

        /* A COMPLETER */

        System.out.println("Projection matrix:\n" + projection);
    }

    public void setCalibration(double focal, double width, double height) {

        /* à compléter */

        System.out.println("Calibration matrix:\n" + calibration);
    }

    /**
     * Projects the given homogeneous, 4 dimensional point onto the screen.
     * The resulting Vector as its (x,y) coordinates in pixel, and its z coordinate
     * is the depth of the point in the camera coordinate system.
     */
    public Vector3 projectPoint(Vector p)
            throws SizeMismatchException, InstantiationException {
        Vector ps = new Vector(3);

        /* à compléter */

        return new Vector3(ps);
    }

    /**
     * Transform a vector from world to camera coordinates.
     */
    public Vector3 transformVector(Vector3 v)
            throws SizeMismatchException, InstantiationException {
        /* Doing nothing special here because there is no scaling */
        Matrix R = worldToCamera.getSubMatrix(0, 0, 3, 3);
        Vector tv = R.multiply(v);
        return new Vector3(tv);
    }

}
