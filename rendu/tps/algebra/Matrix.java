/*
 * @author: cdehais  
 */

package algebra;

import java.lang.Math;

public class Matrix {

    protected Matrix() {
    }

    protected Matrix(String name) {
        this.name = name;
    }

    /**
     * Creates a named Matrix of size nRows x nCols.
     */
    public Matrix(String name, int nRows, int nCols) throws java.lang.InstantiationException {
        this(nRows, nCols);
        this.name = name;
    }

    /**
     * Creates a Matrix of size nRows x nCols.
     */
    public Matrix(int nRows, int nCols) throws java.lang.InstantiationException {
        allocValues(nRows, nCols);
    }

    /**
     * Creates a new 3x3 matrix with the given coefficients
     * 
     * @throws java.lang.InstantiationException
     */
    public Matrix(double a11, double a12, double a13, double a21, double a22, double a23, double a31, double a32,
            double a33) throws java.lang.InstantiationException {
        this(3, 3);
        values[0] = a11;
        values[1] = a12;
        values[2] = a13;
        values[3] = a21;
        values[4] = a22;
        values[5] = a23;
        values[6] = a31;
        values[7] = a32;
        values[8] = a33;
    }

    /**
     * Creates an identity matrix of size @size
     */
    public static Matrix createIdentity(int size) throws java.lang.InstantiationException {
        Matrix id = new Matrix(size, size);

        for (int i = 0; i < size; i++) {
            id.values[size * i + i] = 1.0;
        }
        id.name = "I" + size;
        return id;
    }

    /**
     * Extracts a submatrix of size nRows x nCols with top left corner at
     * (offsetRow, offsetCol)
     */
    public Matrix getSubMatrix(int offsetRow, int offsetCol, int nRows, int nCols)
            throws InstantiationException {
        if ((offsetRow < 0) || (offsetCol < 0) || (nRows < 1) || (nCols < 1) ||
                (offsetRow + nRows > this.nRows) || (offsetCol + nCols > this.nCols)) {
            throw new InstantiationException("Invalid submatrix");
        }

        Matrix sub = new Matrix(nRows, nCols);

        for (int i = 0; i < nRows; i++) {
            for (int j = 0; j < nCols; j++) {
                sub.set(i, j, this.get(offsetRow + i, offsetCol + j));
            }
        }

        return sub;
    }

    /**
     * Transposes the square Matrix.
     */
    public Matrix transpose() {
        Matrix trans;
        try {
            trans = new Matrix(this.nCols, this.nRows);
        } catch (java.lang.InstantiationException e) {
            /* unreached */
            return null;
        }
        for (int i = 0; i < nCols; i++) {
            for (int j = i + 1; j < nRows; j++) {
                trans.values[i * nCols + j] = this.values[j * nCols + i];
                trans.values[j * nCols + i] = this.values[i * nCols + j];
            }
        }
        return trans;
    }

    /**
     * Matrix/Matrix multiplication
     */
    public Matrix multiply(Matrix M) throws SizeMismatchException {
        if (nCols != M.nRows) {
            throw new SizeMismatchException(this, M);
        }

        Matrix R;
        try {
            R = new Matrix(this.nRows, M.nCols);
        } catch (java.lang.InstantiationException e) {
            /* unreached */
            return null;
        }

        for (int i = 0; i < R.nRows; i++) {
            for (int j = 0; j < R.nCols; j++) {
                for (int k = 0; k < this.nCols; k++) {
                    R.values[i * R.nCols + j] += this.values[i * nCols + k] * M.values[k * nRows + j];
                }
            }
        }

        return R;
    }

    /**
     * Multiply by a scalar
     */
    public Matrix multiply(double factor) throws SizeMismatchException {
        Matrix R;
        try {
            R = new Matrix(this.nRows, this.nCols);
        } catch (java.lang.InstantiationException e) {
            /* unreached */
            return null;
        }

        for (int i = 0; i < R.nRows; i++) {
            for (int j = 0; j < R.nCols; j++) {
                R.values[i * R.nCols + j] = this.values[i * nCols + j] * factor;
            }
        }

        return R;
    }

    /**
     * Square
     * Compute a.multiply(a)
     */
    public Matrix square() throws SizeMismatchException {
        return multiply(this);
    }

    /**
     * Matrix/vector multiplication
     */
    public Vector multiply(Vector v) throws SizeMismatchException {
        if (nCols != v.size()) {
            throw new SizeMismatchException(this, v);
        }

        Vector u = null;
        try {
            u = new Vector(nRows);
        } catch (java.lang.InstantiationException e) {
            /* unreached */
        }

        for (int i = 0; i < u.size(); i++) {
            double e = 0.0;
            for (int k = 0; k < this.nCols; k++) {
                e += values[i * nCols + k] * v.get(k);
            }
            u.set(i, e);
        }

        return u;
    }

    /**
     * Matrix addition
     */
    public Matrix add(Matrix other) throws SizeMismatchException {
        try {
            Matrix sum = new Matrix(nRows, nCols);
            for (int i = 0; i < nRows; i++) {
                for (int j = 0; j < nCols; j++) {
                    sum.values[i * nCols + j] = values[i * nCols + j] + other.values[i * nCols + j];
                }
            }
            return sum;
        } catch (InstantiationException e) {

            throw new SizeMismatchException();
        }
    }

    /**
     * Sets the element on row @i and column @j to the given value @value.
     */
    public void set(int i, int j, double value) {
        values[i * nCols + j] = value;
    }

    /**
     * Sets all the elements on a certain column
     * 
     * @param j
     * @return
     */
    public void setColumn(int j, Vector v) throws SizeMismatchException {
        if (v.size() != nRows) {
            throw new SizeMismatchException();
        }
        for (int i = 0; i < nRows; i++) {
            values[i * nCols + j] = v.get(i);
        }
    }

    /**
     * Sets the elements of the submatrix starting a the top left corner specified
     * by (i, j)
     * 
     * @param i
     * @param j
     * @param submatrix
     * @return
     */

    public void setSubmatrix(int i, int j, Matrix submatrix) throws SizeMismatchException {
        if (i + submatrix.nRows > nRows || j + submatrix.nCols > nCols) {
            throw new SizeMismatchException();
        }
        for (int k = 0; k < submatrix.nRows; k++) {
            for (int l = 0; l < submatrix.nCols; l++) {
                values[(i + k) * nCols + j + l] = submatrix.get(k, l);
            }
        }
    }

    /**
     * Gets the element on row @i and column @j.
     */
    public double get(int i, int j) {
        return values[i * nCols + j];
    }

    /**
     * Sets the matrix name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a Matlab compatible representation of the Matrix.
     */
    public String toString() {
        String repr = name + " = [";
        int spacing = repr.length();
        for (int i = 0; i < nRows; i++) {
            if (i > 0) {
                for (int j = 0; j < spacing; j++) {
                    repr += " ";
                }
            }
            for (int j = 0; j < nCols; j++) {
                repr += values[nCols * i + j] + " ";
            }
            repr += ";\n";
        }

        repr += "];";

        return repr;
    }

    protected void allocValues(int nRows, int nCols) throws java.lang.InstantiationException {
        int size = nRows * nCols;
        if (size < 1) {
            throw new java.lang.InstantiationException("Both matrix dimensions must be strictly positive");
        }
        this.values = new double[size];
        this.nRows = nRows;
        this.nCols = nCols;
    }

    public String getName() {
        return name;
    }

    public int nRows() {
        return nRows;
    }

    public int nCols() {
        return nCols;
    }

    public String name = "M";
    protected double values[];
    private int nRows;
    private int nCols;
}
