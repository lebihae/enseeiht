modèles
    discret
    continu
        implicite: f(x, y) = bool
        explicite
            paramétrique: f(params) = x, y
            fonctionnel: f(x) = y => repr pas tout
point vs vecteur
    translater point fait qqch
    translater vecteur fait rien
    coord homogenes (x, y, z, 1 if point else 0)
combinaisons
    affine: sum lambda a avec sum lambda = 1 et a dans points
        lambda = coord affines / barycentriques du point
            /!\ plus lambda PETIT plus on est PROCHE du point que lambda scale
                lambda = influence du point sur la position
transformations
    affine: conserve barycentres
    transl: affine, pas lin
    rot: affine, lin
    repr matricielle: [ matrice d'une AL | t1; t2; t3 ]
courbes
    implicite: (AB) = ker(M |-> AM scalaire n)
    explicite: (AB) = f(espace)
    paramétrisation régulière: forall t, F'(t) != 0 iff forall t, norm F'(t) != 0
        ie pas de points de rebroussement
    .. à abcisse curviligne: forall t, norm F'(t) = 1
    longueur: integral from t_0 to t of norm f'
interpolation
    lagrange
        fonctionnelle: avec (x, y)i les pts de controle:
            F(x) = sum(k) y_k prod(j != i) x-x_j / x_i-x_j
            et on a F(x_i) = y_i
        paramétrique: on veut F polynomiale tq F(t_i) = (x_i, y_i)  avec t_i les temps de contrôle
            F(t) = sum F(t_i) prod(i!=j) t-t_j / t_i-t_m
            choix des temps de controle:
                reguliere, distance, sqrt des distances, techbytechev:
    neville
        fonctionnelle: pour points de ctl (x, y__(0..n):
            P(x) = p_(0, n)(x) = [(x-x_i) p_(i+1, j)(x) - (x-x_j)p_(i,j-1)(x)] / x_j-x_i
                 et p_(i, i) = y_i
approximation   
    bézier
        point(t) = lerp(P_0, P_3)(t) = lerp(lerp(P_0, P_1)(t), lerp(P_2, P_3)(t))
                 = sum P_k B_k^n(t)
                  
        B_k^n(t) = n sum (k parmi n) t^k (1-t)^(n-k)

    courbure
        det(P') / norm^3 P'
    
    hodographe
        P'(t) = n sum (P_(k+1) - P_n) B_k^n(t)
        avec B les bernstein: B_k^n(t) = (k parmi n) (1-t)^(n-k) t^k

    blossom on note p = B[P] la floraison d'un polynôme P
        multi-affine, symm, prop diagonale: p(t t t … t) = P(t)
        en notant P_k les pts de ctl de la courbe
        P_k = p(0 0 0 … (n - k fois) … 0 1 … (k fois) … 1)
        lerp(P_0, P_1, t) = p(
        b(t1 t2 … t_n) = 

        p(t1 ... tn) = sum ((I, J) partition de {1…n} )  prod_I (t-t_i / t-
    
        
            
    
    
