import math
from functools import reduce
from typing import Iterable, TypeAlias, TypeVar

import numpy as np

################################################################################################
###################################### Interpolation - FONCTIONNEL ####################################
################################################################################################


def prod(l):
    return reduce(lambda x, y: x * y, l, 1)


def lagrange(XX: list[float], YY: list[float], x_int: float):
    #  fonction : lagrange
    #  semantique : calcule la valeur en x_int du polynome de Lagrange passant
    #               par les points de coordonnées (XX,YY)
    #  params :
    #           - float x_int : point dont on cherche l'ordonnée
    #           - List<float> XX : liste des abscisses des points
    #           - List<float> YY : liste des ordonnées des points
    #  sortie : valeur en x_int du polynome de Lagrange passant
    #           par les points de coordonnées (XX,YY)
    n = len(XX)

    return sum(
        YY[i] * prod((x_int - XX[j]) / (XX[i] - XX[j]) for j in range(n) if i != j)
        for i in range(n)
    )


################################################################################################
###################################### Interpolation - PARAMETRIQUE ###################################
################################################################################################


def lagrange_param(
    XX: list[float], YY: list[float], TT: list[float], list_tt: list[float]
):
    #  fonction : lagrange_param
    #  semantique : applique la subdivion de Lagrange aux points (XX, YY)
    #               placés en paramètres en suivant les temps indiqués
    #  params :
    #           - List<float> XX : liste des abscisses des points
    #           - List<float> YY : liste des ordonnées des points
    #           - List<float> TT : temps de la subdivision
    #           - List<float> list_tt : échantillon des temps sur TT
    #  sortie : points (xx, yy) de la courbe interpolée
    #

    points = [lagrange(XX, YY, t / TT[-1]) for t in list_tt]
    # return [t / TT[-1] for t in list_tt], points

    points_x = []
    points_y = []

    for t in list_tt:
        points_x.append(lagrange(TT, XX, t))
        points_y.append(lagrange(TT, YY, t))
    
    return points_x, points_y


T = TypeVar("T")
def flat(t: list[list[T]]) -> list[T]:
    ret = []
    for sublist in t:
        ret.extend(sublist)
    return ret

def parametrisation_reguliere(
    nb_elt: int, pas_tps: float
) -> tuple[list[float], list[float]]:
    #  fonction : parametrisation_reguliere
    #  semantique : construit la subdivision reguliere et les échantillons
    #               de temps selon cette subdivision
    #  params :
    #           - int nb_elt : nombre d'elements de la subdivision
    #           - float pas_tps : pas d'échantillonage
    #  sortie :  - List<float> T : subdivision reguliere
    #            - List<float> t_to_eval : echantillon sur la subdivision

    T = [float(e) for e in range(nb_elt)]
    # step_size = math.floor(len(T) / pas_tps)
    evals = [0.0]
    while evals[-1] < T[-1]:
        evals.append(evals[-1] + pas_tps)
    ret = T, evals
    print(f"regulière({nb_elt=}, {pas_tps=}) = {ret}")
    return ret


def norm(x, y):
    return math.sqrt(x**2 + y**2)


def distance(p1: tuple[float, float], p2: tuple[float, float]) -> float:
    x1, y1 = p1
    x2, y2 = p2

    return norm(x2 - x1, y2 - y1)


def parametrisation_distance(
    nb_elt: int, pas_tps: float, XX: list[float], YY: list[float]
):
    #  fonction : parametrisation_distance
    #  semantique : construit la subdivision sur les distances et les échantillons
    #               de temps selon cette subdivision
    #  params :
    #           - int nb_elt : nombre d'elements de la subdivision
    #           - float pas_tps : pas d'échantillonage
    #  sortie :  - List<float> T : subdivision reguliere
    #            - List<float> tToEval : echantillon sur la subdivision

    points = list(zip(XX, YY))
    print(f"{points = }")
    T = [0.0]
    for i in range(1, nb_elt):
        T.append(T[-1] + distance(points[i], points[i - 1]))
    return T, np.arange(min(T), max(T), pas_tps)


def parametrisation_racinedistance(
    nb_elt: int, pas_tps: float, XX: list[float], YY: list[float]
):
    #  fonction : parametrisation_reguliere
    #  semantique : construit la subdivision sur les racines distances et les échantillons
    #               de temps selon cette subdivision
    #  params :
    #           - int nb_elt : nombre d'elements de la subdivision
    #           - float pas_tps : pas d'échantillonage
    #  sortie :  - List<float> T : subdivision reguliere
    #            - List<float> tToEval : echantillon sur la subdivision

    points = list(zip(XX, YY))
    T = [0.0]
    for i in range(1, nb_elt):
        T.append(T[-1] + math.sqrt(distance(points[i], points[i - 1])))
    return T, np.arange(min(T), max(T), pas_tps)


def parametrisation_Tchebychev(nb_elt, pas_tps):
    #  fonction : subdivision_reguliere
    #  semantique : construit la subdivision basée sur Tchebycheff et les échantillons
    #               de temps selon cette subdivision
    #  params :
    #           - int nb_elt : nombre d'elements de la subdivision
    #           - float pas_tps : pas d'échantillonage
    #  sortie :  - List<float> T : subdivision reguliere
    #            - List<float> tToEval : echantillon sur la subdivision

    T = [math.cos(((2 * i + 1) * math.pi) / (2 * nb_elt + 2)) for i in range(nb_elt)]
    return T, np.arange(min(T), max(T), pas_tps)


T = TypeVar("T")


def neighbors(o: Iterable[T]) -> Iterable[tuple[T, T]]:
    l = list(o)
    for i in range(len(l)):
        yield l[i], l[i + 1 if i + 1 < len(l) else 0]


def neville(XX: list[float], YY: list[float], TT: list[float], tt: float):
    #  fonction : neville
    #      semantique : calcule le point atteint par la courbe en tt sachant
    #                   qu'elle passe par les (XX,YY) en TT
    #      params :
    #               - List<float> XX : liste des abscisses des points
    #               - List<float> YY : liste des ordonnées des points
    #               - List<float> TT : liste des temps de la subdivision
    #               - tt : temps ou on cherche le point de la courbe
    #      sortie : point atteint en t de la courbe

    assert len(XX) == len(YY) == len(TT)

    def aux(i, j, axis):
        if abs(i-j) == 1:
            return (1 - tt) * axis[i] + tt * axis[j]
        return (TT[j] - tt) / (TT[j] - TT[i]) * aux(i, j-1, axis) + (tt - TT[i]) / (TT[j] - TT[i]) * aux(i+1, j, axis)

    # return x, y
    return 0, 0


def neville_param(XX, YY, TT, list_tt):
    # fonction : neville_param
    #  semantique : applique la subdivion de Neville aux points (XX,YY)
    #               placés en paramètres en suivant les temps indiqués
    #  params :
    #           - List<float> XX : liste des abscisses des points
    #           - List<float> YY : liste des ordonnées des points
    #           - List<float> TT >: temps de la subdivision
    #           - List<float> list_tt : échantillon des temps sur TT
    #  sortie : points (xx,yy) de la courbe interpolée

    xx, yy = [], []

    points = [neville(XX, YY, TT, tt) for tt in list_tt]

    return [x for x, _ in points], [y for _, y in points]


################################################################################################
###################################### Interpolation - SURFACE FONCTIONNEL #####################
################################################################################################


def interpolate_surface(XX, YY, ZZ, TT, list_tt, nb_point_grille):
    #  fonction : interpolate_surface
    #      semantique : calcule le point atteint par la surface en tt sachant
    #                   qu'elle passe par les (XX,YY) en TT
    #      params :
    #               - Array<float> XX : coorodnnées X des points 3D
    #               - Array<float> YY : coorodnnées Y des points 3D
    #               - Array<float> ZZ : coorodnnées Z des points 3D
    #               - List<float> TT : temps de la subdivision
    #               - List<float> list_tt : échantillon des temps sur TT
    #      sortie : (Array<float> points 3D des courbes interpolées en fixant Y,
    #                   Array<float> points 3D des courbes interpolées en fixant X)

    
    return 0, 0
