from pathlib import Path

from catmullclark import catmull_clark
from main import cube

here = Path(__file__).parent
for n in range(5):
    catmull_clark(cube, n).render(to=here / f"catmulled_{n}.png", faces=n >= 3)
