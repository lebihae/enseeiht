from pathlib import Path

from matplotlib import pyplot as plt

from catmullclark import catmull_clark
from mesh import Mesh

plt.close("all")

cube = Mesh.empty(id_size=5)

face_up = cube.make_face(name="up")
face_down = cube.make_face(name="down")
face_left = cube.make_face(name="left")
face_right = cube.make_face(name="right")
face_front = cube.make_face(name="front")
face_back = cube.make_face(name="back")

cube.add_vertice((0, 0, 0), (0, 1, 0), face_down)
cube.add_vertice((0, 1, 0), (1, 1, 0), face_down)
cube.add_vertice((1, 1, 0), (1, 0, 0), face_down)
cube.add_vertice((1, 0, 0), (0, 0, 0), face_down)

cube.add_vertice((0, 0, 1), (0, 1, 1), face_up)
cube.add_vertice((0, 1, 1), (1, 1, 1), face_up)
cube.add_vertice((1, 1, 1), (1, 0, 1), face_up)
cube.add_vertice((1, 0, 1), (0, 0, 1), face_up)

cube.add_vertice((0, 0, 0), (0, 0, 1), face_left)
cube.add_vertice((0, 0, 1), (0, 1, 1), face_left)
cube.add_vertice((0, 1, 1), (0, 1, 0), face_left)
cube.add_vertice((0, 1, 0), (0, 0, 0), face_left)

cube.add_vertice((1, 0, 0), (1, 0, 1), face_right)
cube.add_vertice((1, 0, 1), (1, 1, 1), face_right)
cube.add_vertice((1, 1, 1), (1, 1, 0), face_right)
cube.add_vertice((1, 1, 0), (1, 0, 0), face_right)

cube.add_vertice((0, 0, 0), (0, 0, 1), face_front)
cube.add_vertice((0, 0, 1), (1, 0, 1), face_front)
cube.add_vertice((1, 0, 1), (1, 0, 0), face_front)
cube.add_vertice((1, 0, 0), (0, 0, 0), face_front)

cube.add_vertice((0, 1, 0), (0, 1, 1), face_back)
cube.add_vertice((0, 1, 1), (1, 1, 1), face_back)
cube.add_vertice((1, 1, 1), (1, 1, 0), face_back)
cube.add_vertice((1, 1, 0), (0, 1, 0), face_back)

if __name__ == "__main__":
    here = Path(__file__).parent
    cube.export(to=here / "cube.json")
    cube.render(to=here / "cube.png")

    # try:
    catmulled = catmull_clark(cube)
    catmulled.render(to=here / "catmulled.png")
    catmulled.export(to=here / "catmulled.json")
    catmulled.show()
    # finally:
    #     plt.show()
