import typing
from copy import deepcopy
from os import PathLike
from pathlib import Path
from typing import Iterable

import matplotlib.pyplot as plt
import nanoid
import nanoid.algorithm
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

import lib
from lib import ID, Edge, Point, Tag


class Mesh:
    points: dict[ID, Point]
    # les vertices sont des paires de points
    vertices: list[tuple[ID, ID]]
    # les faces stockent l'ensemble des point qui la constitue
    faces: dict[ID, list[ID]]
    # données custom sur chacun des points
    tags: dict[ID, Tag]

    id_size: int
    id_prefix: str = ""

    def _makeid(self) -> ID:
        return self.id_prefix + nanoid.generate(
            size=self.id_size,
            alphabet="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
        )

    @classmethod
    def empty(cls, id_size=20, id_prefix="") -> "Mesh":
        return cls(
            points={},
            vertices=[],
            faces={},
            id_size=id_size,
            tags={},
            id_prefix=id_prefix,
        )

    def __str__(self) -> str:
        return (
            f"Mesh(points={self.points}, vertices={self.vertices}, faces={self.faces})"
        )

    def __init__(
        self,
        points: dict[ID, Point],
        vertices: list[tuple[ID, ID]],
        faces: dict[ID, list[ID]],
        tags: dict[ID, Tag],
        id_size=20,
        id_prefix="",
    ):
        self.points = points
        self.vertices = vertices
        self.faces = faces
        self.id_size = id_size
        self.tags = tags
        self.id_prefix = id_prefix

    def find_point(self, point: Point) -> ID | None:
        for id, p in self.points.items():
            if p == point:
                return id
        return None

    def tags_of(self, id: ID) -> Tag:
        return self.tags.get(id, {})

    def points_with_tags(self, **tag_matches) -> Iterable[ID]:
        """
        AND equality matches. Use None to check for absence of tag.
        """
        for id, tags in self.tags.items():
            if all(tags.get(k) == v for k, v in tag_matches.items()):
                yield id

    def attach_to_face(self, point_id: ID, face_id: ID):
        if face_id not in self.faces:
            raise ValueError("Face does not exist")
        if point_id not in self.points:
            raise ValueError("Point does not exist")
        if point_id in self.faces[face_id]:
            return
        self.faces[face_id].append(point_id)

    def connect_to_vertice(self, start: ID, end: ID):
        if (start, end) in self.vertices:
            return
        self.vertices.append((start, end))

    def add_point(
        self, point: Point, face_id: ID | None = None, **tags: typing.Any
    ) -> ID:
        id = self.find_point(point)
        lib.d(f"found {id} for {point} within {(self.points.keys())}")
        found = id is not None
        id = id or self._makeid()

        if not found:
            self.points[id] = point

        self.tags[id] = tags

        if face_id is not None:
            self.attach_to_face(id, face_id)

        if found:
            return id

        return id

    def add_points(
        self, points: Iterable[Point], face_id: ID | None = None, **tags: typing.Any
    ) -> list[ID]:
        return [self.add_point(p, face_id, **tags) for p in points]

    def add_vertice(
        self, start: Point, end: Point, face_id: ID | None = None
    ) -> tuple[ID, ID]:
        lib.dc("add_vertice", start, end, face_id)
        id_start = self.add_point(start, face_id)
        id_end = self.add_point(end, face_id)
        lib.d(f"using {id_start} and {id_end} for {start} and {end}")
        if (id_start, id_end) in self.vertices or (id_end, id_start) in self.vertices:
            return (id_start, id_end)
        self.vertices.append((id_start, id_end))

        return (id_start, id_end)

    def name(self, id: ID | Point | tuple[ID | Point, ID | Point]) -> str:
        if isinstance(id, tuple):
            return f"{self.name(id[0])}-{self.name(id[1])}"

        if id is not None and not isinstance(id, str):
            found = self.find_point(id)
            if not found:
                return "unknown"
            id = found

        return self.tags.get(id, {}).get("name", id)

    def names(self, *ids: Point | ID | tuple[ID | Point, ID | Point]) -> list[str]:
        return [self.name(id) for id in ids]

    def display(self, p: Point) -> str:
        return f"{p}@{self.find_point(p)}"

    def face_vertices(self, face_id: ID) -> Iterable[tuple[ID, ID]]:
        for corner_id in self.faces[face_id]:
            for vertice in self.vertices:
                if corner_id in vertice:
                    yield vertice

    def neighbour_faces(
        self, edge_start: Point, edge_end: Point, mesh: "Mesh | None" = None
    ) -> Iterable[ID]:
        for id in self.faces:
            matching_vertices = [
                vertice
                for vertice in self.face_vertices(id)
                if {edge_start, edge_end} == set(self.resolve_all(vertice))
            ]

            if len(matching_vertices) >= 2:
                yield id

        # lib.dc("neighbour_faces", self.display(edge_start), self.display(edge_end))
        # for face_id in self.faces.keys():
        #     print(f"trying {self.name(face_id)}")
        #     points_in_face: list[Point] = []
        #     for point in self.resolve_face(face_id):
        #         if lib.is_on_edge(
        #             (edge_start, edge_end),
        #             point,
        #             names=(mesh or self).names(point, edge_start, edge_end),
        #         ):
        #             points_in_face.append(point)
        #     if len(points_in_face) >= 2:
        #         print(f"has {[self.name(p) for p in points_in_face]}")
        #         yield face_id

    def faces_touching(self, point: ID) -> Iterable[ID]:
        for face_id, face in self.faces.items():
            if point in face:
                yield face_id

    def edges_touching(self, point: ID) -> Iterable[tuple[ID, ID]]:
        for vertice in self.vertices:
            if point in vertice:
                yield vertice

    def make_face(self, *points: ID, **tags: typing.Any) -> ID:
        id = self._makeid()
        if id in self.faces:
            raise ValueError("ID already exists")

        self.faces[id] = list(points)
        self.tags[id] = tags
        return id

    def copy(self) -> "Mesh":
        return Mesh(
            points=deepcopy(self.points),
            vertices=deepcopy(self.vertices),
            faces=deepcopy(self.faces),
            tags=deepcopy(self.tags),
        )

    def resolve(self, id: ID) -> Point:
        return self.points[id]

    def resolve_face(self, face_id: ID) -> list[Point]:
        return self.resolve_all(self.faces[face_id])

    def resolve_all(self, ids: Iterable[ID]) -> list[Point]:
        return [self.points[i] for i in ids]

    def resolve_edges(self, ids: Iterable[tuple[ID, ID]]) -> list[Edge]:
        return [(self.resolve(start), self.resolve(end)) for start, end in ids]

    def has_edge_with(self, start: ID, end: ID) -> bool:
        return (start, end) in self.vertices or (end, start) in self.vertices

    def infer_faces(self):
        """
        Get all cycles of size 4 in the vertices
        Make faces out of them.
        """

        graph = {
            id: {o for o in self.points if self.has_edge_with(id, o)}
            for id in self.points
        }

        for cycle in lib.find_cycles(graph, 4):
            self.make_face(*cycle)

    def _matplotlib_render(self, faces_alpha=0.25):
        fig = plt.figure()
        ax = fig.add_subplot(projection="3d")
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        ax.set_zlabel("Z")  # type: ignore

        print(f"Rendering with {len(self.points)} points")
        if len(self.points) < 200:
            xs = []
            ys = []
            zs = []
            for x, y, z in self.points.values():
                xs.append(x)
                ys.append(y)
                zs.append(z)

            for start, end in self.resolve_edges(self.vertices):
                x, y, z = start
                end_x, end_y, end_z = end
                ax.plot([x, end_x], [y, end_y], [z, end_z])

            ax.scatter(xs, ys, zs)

            if len(self.points) < 50:
                for id, (x, y, z) in self.points.items():
                    ax.text(x, y, z, self.name(id))  # type: ignore

        def random_color_hex(opacity: float) -> str:
            import random

            return f"#{random.randint(0, 0xFFFFFF):06x}{int(opacity*255):02x}"

        if faces_alpha > 0:
            for id in self.faces:
                ax.add_collection3d(
                    Poly3DCollection(
                        verts=[self.resolve_face(id)],
                        facecolors=[random_color_hex(faces_alpha)],
                    )
                )

    def show(self, faces: float | bool = False):
        self._matplotlib_render(faces_alpha=float(faces))
        plt.show()

    def render(self, to: str | Path, faces: float | bool = False):
        self._matplotlib_render(faces_alpha=float(faces))
        plt.savefig(to)

    def move(self, id: ID, to: Point):
        self.points[id] = to

    def translate(self, id: ID, by: Point):
        self.points[id] = lib.translated(self.points[id], by)

    @property
    def frozen(self) -> dict:
        return {
            "points": self.points,
            "vertices": self.vertices,
            "faces": self.faces,
            "tags": self.tags,
        }

    def export(self, to: Path | str):
        import json
        from pathlib import Path

        Path(to).write_text(json.dumps(self.frozen, indent=2))

    @classmethod
    def load(cls, file: Path | str) -> "Mesh":
        import json
        from pathlib import Path

        data = json.loads(Path(file).read_text())
        result = cls.empty()
        result.points = data.get("points", {})
        result.vertices = data.get("vertices", [])
        result.faces = data.get("faces", {})
        result.tags = data.get("tags", {})
        return result
