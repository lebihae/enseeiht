from math import log

import lib
from lib import ID, Point, Tag
from mesh import Mesh


def catmull_clark(input: Mesh, iterations=1) -> Mesh:
    if iterations == 0:
        return input
    return catmull_clark(catmull_clark_one(input), iterations - 1)


def catmull_clark_one(input: Mesh) -> Mesh:
    catmulled = Mesh.empty(id_size=int(3 + log(len(input.points))), id_prefix="C_")
    for face in input.faces:
        catmulled.add_point(
            lib.average(input.resolve_face(face)), original_face=face, nature="face"
        )

    for vertice in input.vertices:
        start, end = input.resolve_all(vertice)
        neighbouring_faces_ids = lib.uniques(list(input.neighbour_faces(start, end)))
        neighbouring_faces = [
            input.resolve_face(face) for face in neighbouring_faces_ids
        ]
        # if len(neighbouring_faces) != 2:
        #     raise ValueError(
        #         #     print(
        #         f"Edge {vertice} (at {[p for p in input.resolve_all(vertice)]}) is connected to {len(neighbouring_faces)} != 2 faces!\nfaces: {[
        #             input.tags_of(face).get("name", input.resolve_face(face)) for face in neighbouring_faces_ids
        #         ] }"
        #     )

        catmulled.add_point(
            lib.average(
                [
                    lib.average(neighbouring_faces[0]),
                    lib.average(neighbouring_faces[1]),
                    start,
                    end,
                ]
            ),
            faces=neighbouring_faces_ids,
            original_vertice=vertice,
            nature="edge",
        )

    for id, point in input.points.items():
        faces_touching_ids = list(input.faces_touching(id))
        faces_touching = [input.resolve_face(face_id) for face_id in faces_touching_ids]
        edges_touching_ids = list(input.edges_touching(id))
        edges_touching = input.resolve_edges(edges_touching_ids)

        if not faces_touching:
            print(f"No faces touching point {point}")
            continue

        if not edges_touching:
            print(f"No edges touching point {point}")
            continue

        mean_touching_faces_averages = lib.average(
            lib.average(face) for face in faces_touching
        )
        average_of_midpoints = lib.average(lib.average(edge) for edge in edges_touching)

        n = len(faces_touching)

        catmulled.add_point(
            lib.scale(
                lib.sum_(
                    mean_touching_faces_averages,
                    lib.scale(average_of_midpoints, 2),
                    lib.scale(point, n - 3),
                ),
                1 / n,
            ),
            faces=faces_touching_ids,
            edges=edges_touching_ids,
            nature="vertex",
        )

    # create edges between every average face point and its edges
    for face_point in catmulled.points_with_tags(nature="face"):
        original_face_id = catmulled.tags_of(face_point)["original_face"]
        edges = [
            p
            for p in catmulled.points_with_tags(nature="edge")
            if original_face_id in catmulled.tags_of(p)["faces"]
        ]
        for edge in edges:
            catmulled.connect_to_vertice(face_point, edge)
        # print(
        #     [{"f": original_face_id, "id": p, **catmulled.tags_of(p)} for p in edges]
        # )

    # create edges between every vertex point and the points that share an edge with it
    for vertex_point in catmulled.points_with_tags(nature="vertex"):
        lib.d(f"VX {vertex_point}")
        for vertex_original_edge in catmulled.tags_of(vertex_point)["edges"]:
            lib.d(f"\t{vertex_original_edge = }")
            for edge in catmulled.points_with_tags(nature="edge"):
                edge_original_vertice = catmulled.tags_of(edge)["original_vertice"]
                lib.d(f"\t\t{edge}: {edge_original_vertice}")
                if edge_original_vertice == vertex_original_edge:
                    lib.d(f"\t\t\t{edge} -> {vertex_point}!!!")
                    catmulled.connect_to_vertice(vertex_point, edge)

    # make faces between where the edges meet
    # for (start_id, end_id) in catmulled.vertices:

    catmulled.infer_faces()

    return catmulled
