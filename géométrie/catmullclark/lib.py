import typing
from typing import Iterable, TypeVar

TOLERANCE = 1e-3
type Point = tuple[float, float, float]
type Tag = dict[str, typing.Any]
type Edge = tuple[Point, Point]
POINTS: list[Point] = []
type ID = str
DEBUG = False


def d(fmt: str, *args, **kwargs):
    if DEBUG:
        print(fmt, *args, **kwargs)


def dc(function_name: str, *args, **kwargs):
    allargs = {i: v for i, v in enumerate(args)} | kwargs
    d(
        f"{function_name}({', '.join(f'{k}={v!r}' if isinstance(k, str) else repr(v) for k, v in allargs.items())})"
    )


def dcret(function_name: str, ret: typing.Any, *args, **kwargs):
    allargs = {i: v for i, v in enumerate(args)} | kwargs
    d(
        f"{function_name}({', '.join(f'{k}={v!r}' if isinstance(k, str) else repr(v) for k, v in allargs.items())}) = {ret!r}"
    )


def is_on_edge(
    edge: tuple[Point, Point], point: Point, names: list[str] | None = None
) -> bool:
    names_ = {i: v for i, v in enumerate(names)} if names else {}
    distance = point_distance_to_line(point, edge)
    dcret(
        "point_distance_to_line",
        distance,
        names_.get(0, point),
        names_.get(1, edge[0]),
        names_.get(2, edge[1]),
    )
    return distance < TOLERANCE


def point_distance_to_line(point: Point, line: tuple[Point, Point]) -> float:
    x0, y0, z0 = point
    x1, y1, z1 = line[0]
    x2, y2, z2 = line[1]

    return (
        abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1)
        / ((y2 - y1) ** 2 + (x2 - x1) ** 2 + (z2 - z1) ** 2) ** 0.5
    )


def normalized(point: Point) -> Point:
    return scale(point, 1 / norm(point))


def scale(point: Point, factor: float) -> Point:
    return (point[0] * factor, point[1] * factor, point[2] * factor)


def norm(point: Point) -> float:
    x, y, z = point
    return (x**2 + y**2 + z**2) ** 0.5


def distance(a: Point, b: Point) -> float:
    return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2 + (a[2] - b[2]) ** 2) ** 0.5


def average(points: Iterable[Point]) -> Point:
    ps = list(points)
    n = len(ps)
    x = sum(p[0] for p in ps) / n
    y = sum(p[1] for p in ps) / n
    z = sum(p[2] for p in ps) / n
    return (x, y, z)


def difference(a: Point, b: Point) -> Point:
    """
    Returns a - b
    """
    return (a[0] - b[0], a[1] - b[1], a[2] - b[2])


def translated(a: Point, b: Point) -> Point:
    """
    Returns a + b
    """
    return (a[0] + b[0], a[1] + b[1], a[2] + b[2])


def sum_(*points: Point) -> Point:
    if len(points) == 0:
        return (0, 0, 0)
    if len(points) == 1:
        return points[0]
    if len(points) == 2:
        return translated(*points)
    return translated(points[0], sum_(*points[1:]))


T = typing.TypeVar("T")


def uniques(items: list[T]) -> list[T]:
    """
    Does not presever order
    """
    return list(set(items))


T = TypeVar("T")


def find_cycles_recursive(graph: dict[T, set[T]], length: int, cycle: list[T]):
    successors = graph[cycle[-1]]
    if len(cycle) == length:
        if cycle[0] in successors:
            yield cycle
    elif len(cycle) < length:
        for v in successors:
            if v in cycle:
                continue
            yield from find_cycles_recursive(graph, length, cycle + [v])


def find_cycles(graph: dict[T, set[T]], length: int):
    for v in graph:
        yield from find_cycles_recursive(graph, length, [v])
