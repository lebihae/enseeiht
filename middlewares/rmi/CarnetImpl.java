import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class CarnetImpl extends UnicastRemoteObject implements Carnet {
    protected CarnetImpl() throws RemoteException {
        super();
    }

    private HashMap<String, SFiche> fiches = new HashMap<>();

    public void Ajouter(SFiche sf) throws RemoteException {
        this.fiches.put(sf.getNom(), sf);
    }

    public RFiche Consulter(String n, boolean forward) throws RemoteException {
        SFiche sfiche = this.fiches.get(n);
        return new RFicheImpl(sfiche.getNom(), sfiche.getEmail());
    }

    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(6666);
            Carnet c = new CarnetImpl();
            Naming.bind("//localhost:6666/quoicoubeh", c);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
