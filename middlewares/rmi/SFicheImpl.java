public class SFicheImpl implements SFiche {
    private String nom;
    private String email;

    public SFicheImpl(String nom, String email) {
        this.email = email;
        this.nom = nom;
    }

    public String getNom() {
        return this.nom;
    }

    public String getEmail() {
        return this.email;
    }
}
