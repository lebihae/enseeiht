#!/usr/bin/env fish
set black_color "#202124"
set white_color "#ffffff"

set here (dirname (status --current-filename))

for figure in $here/*/figures/*.svg
	sd $white_color "#000000" "$figure"
	sd $black_color "#ffffff" "$figure"
	# sd '\bstroke-width:2\b' 'stroke-width:20' "$figure"
	convert $figure (dirname $figure)/(basename $figure .svg).png
	rm $figure
end

# pdflatex -output-directory= -interaction=batchmode $here/*/*.tex

